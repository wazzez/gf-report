<?php
header_remove('Set-Cookie');
session_start();
// $progress = $_SESSION["progress"] * 100;
// echo "$progress";
header("Content-Type: text/event-stream\n\n");
header("Cache-Control: no-cache");
header("Pragma: no-cache");

function progress($id){
    if (!isset($_SESSION["progress"])) {
        return;
    }

    $progress = $_SESSION["progress"];
    ob_start();
    error_log($progress);
    echo "id: $id\n";
    echo "event: progress\n";
    $json_payload = array('progress' => $progress*100);
    echo 'data: ' . json_encode( $json_payload ) . "\n\n";
    ob_flush();
    flush();
    if ($progress >= 1) {
        session_write_close();
        die();
    }
}
$serverTime = time();
progress($serverTime);
session_write_close();
 ?>
