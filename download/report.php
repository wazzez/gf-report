<?php

include_once('../src/server/writer/ListWriter.php');

if (!isset($_GET['date']) && !isset($_GET['option'])) {
	die('Cannot download file');
}

exportExcel($_GET['date'], $_GET['option']);