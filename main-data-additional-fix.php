<?php
require_once('./src/client/fetcher/Main_Data_Agency_Fix_Fetcher.php');
$fetcher = new Main_Data_Agency_Fix_Fetcher;
if (!empty($_GET['limit'])) {
    $limit = $_GET['limit'];
} else {
    $limit = 20;
}
$title = "แก้ไขรายการข้อมูลหลัก";
include('./header.php');
?>
    <div class="jumbotron">
      <div class="container">
        <h2>เพิ่มรายการหลักที่มีอยู่ในข้อมูลหลัก(เพิ่ม)</h2>
      </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="main-data-additional-fix-submit.php" method="post" accept-charset="utf-8">
                <input type="submit" class="btn btn-default" name="submit" value="Submit" style="margin-bottom:20px;"/>
                <div class="col-md-12">
                    <table class="table table-condensed table-bordered">
                        <thead>
                            <th>รหัสงบประมาณ</th>
                            <th width=250>ผลผลิตโครงการ</th>
                            <th width=200>ชื่อรายการ</th>
                            <th>จำนวนเงิน</th>
                            <th>รหัสหน่วยเบิก</th>
                            <th>ชื่อหน่วยเบิก</th>
                            <th>ปีงบประมาณ</th>
                            <th>รหัสคำขอ</th>
                            <th>ยืนยัน</th>
                        </thead>
                        <tbody id='content'>
                            <? $fetcher->displayHTML(1, $limit); ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-offset-5 col-md-1">
                <select id="page-control" class="form-control">
                  <option value='20' <?php echo $limit==20 ? "selected" : "" ?>>20</option>
                  <option value='50' <?php echo $limit==50 ? "selected" : "" ?>>50</option>
                  <option value='100' <?php echo $limit==100 ? "selected" : "" ?>>100</option>
                  <option value='all' <?php echo $limit=='all' ? "selected" : "" ?>>ทั้งหมด</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div id="page-selection"></div>
        </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/jquery.bootpag.min.js"></script>
    <script src="js/main.js"></script>

    <script type="text/javascript">
        $(".row-data td input").change(function() {
            var id = $(this).data("id");
            $("#validated-"+id).prop("checked", true);
        });
    </script>

    <script>
        // init bootpag
        $('#page-selection').bootpag({
            total: <? echo $fetcher->numberOfPages($limit); ?>,
            maxVisible: 20,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
        }).on("page", function(event, /* page number here */ num){
            loadpage(num, <? echo $limit; ?>);
        });

        $('#page-control').change(function() {
            var limit = limit = $(this).val();
            window.location.href = "/main-data-additional-fix.php?limit="+limit;
        });

        function loadpage(page, limit) {
            $("#content").load('main-data-additional-fix-ajax.php?page='+page+'&limit='+limit , function() {
                $('#page-selection').bootpag({total:  <? echo $fetcher->numberOfPages($limit); ?>});
            });
        }
    </script>
    <script>
