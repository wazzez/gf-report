<?php

include_once('src/server/database/database.php');
include_once('src/client/fetcher/GF_Main_Date_Fix_Fetcher.php');
require_once('./src/client/fetcher/Main_Data_Fix_Fetcher.php');

if(isset($_POST) && count($_POST) > 0) {
    foreach($_POST as $key=>$value) {
        if(strpos($key, 'lid-') === FALSE) { continue; }
        $id_componet = explode('-',$key);
        if(count($id_componet) != 2) { continue; }
        $id = $id_componet[1];

        if(isset($_POST["validated-$id"])) {
            $conn = Database::connect();
            $query = "UPDATE gf_list_data
                        SET list_id = $value, validated = true
                        WHERE  id = $id";
            if(!$conn->query($query)) {
                die('update gf error: '.$conn->error);
            }
        }
    }

    $fetcher = new GF_Main_Date_Fix_Fetcher;
    $mainFixFetcher = new Main_Data_Fix_Fetcher;
    if($fetcher->numberOfItems() > 0) {
        header("Location: gf-main-data-fix.php");
    } elseif ($mainFixFetcher->numberOfItems() > 0) {
        header("Location: main-data-fix.php");
    } else {
        header("Location: gf-main-data.php");
    }

	die();
}
