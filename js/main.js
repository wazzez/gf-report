function handleProgress() {
  $(".progress-container").show();
  var flag = true;
  var source = new EventSource("progress.php");
  source.addEventListener("progress", function(event) {
    if (!flag) {
      var data = JSON.parse(event.data);
      var progress = Math.floor(data.progress);
      console.log(progress);
      $('.progress-bar').attr('aria-valuenow', progress).css('width',progress + "%").text(progress + "%");
      if (progress >= 100) {
          source.close();
      }
    }
    flag = false;

  }, false);
}
