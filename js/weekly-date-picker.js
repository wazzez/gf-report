//Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
var firstDate;
var lastDate;
$("#weekly-date-picker").datetimepicker({
    format: 'YYYY-MM-DD'
});

//Get the value of Start and End of Week
$('#weekly-date-picker').on('dp.change', function (e) {
    var value = $("#weekly-date-picker").val();
    firstDate = moment(value, "YYYY-MM-DD").day(0).format("YYYY-MM-DD");
    lastDate =  moment(value, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
    $("#weekly-date-picker").val(firstDate + " - " + lastDate);
});

var today = moment().format("YYYY-MM-DD");
firstDate = moment(today, "YYYY-MM-DD").day(0).format("YYYY-MM-DD");
lastDate =  moment(today, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
$("#weekly-date-picker").val(firstDate + " - " + lastDate);
