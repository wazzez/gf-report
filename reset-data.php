<?php
require_once('src/server/database/database.php');

if (isset($_POST) && isset($_POST['reset'])) {
    if($_POST['reset'] == true) {
        resetData();
    }
}
function resetData() {
    $conn = Database::connect();

    $query = "TRUNCATE TABLE gf_list_data;
              TRUNCATE TABLE list_agency_main_data;
              TRUNCATE TABLE list_main_data;
              TRUNCATE TABLE list_project;
              TRUNCATE TABLE tmp_list_main_data;";
    if(!$conn->multi_query($query)) {
        die($conn->error);
    }
}

$title = 'ล้างข้อมูล';
include('header.php');
?>

    <div class="jumbotron">
        <div class="container">
        <a id="reset-button" class="btn">ล้างข้อมูล</a>
        </div>
    </div>
    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>


    <script type="text/javascript">
        $('#reset-button').click(function() {
            $.ajax({
              method: "POST",
              url: "/reset-data.php",
              data: { reset: true }
            })
              .done(function( msg ) {
                alert( "ล้างข้อมูลเรียบร้อย" );
              });
        });
    </script>
</body>
</html>
