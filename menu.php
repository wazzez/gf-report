        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a class="navbar-brand" href="#">Brand</a>-->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a class="navbar-brand" href="./">Home</a></li>
                    <li><a class="navbar-brand" href="./list-main-data.php">รายการหลัก</a></li>
                    <li><a class="navbar-brand" href="./gf-main-data.php">input รายการหลัก</a></li>
                    <li class="dropdown">
                        <a href="#" class="navbar-brand dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายงาน<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">สรุป</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="./report-list-summary.php?weekly-date-picker=<?=$weekly_query?>">ภาพรวม</a></li>
                            <li><a href="./report-list-summary.php?option=remain&weekly-date-picker=<?=$weekly_query?>">เงินเหลือจ่าย</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="./report-list-summary.php?option=a-1&weekly-date-picker=<?=$weekly_query?>">รายการที่ยังไม่ทำสัญญา น้อยกว่าสองล้านบาท</a></li>
                            <li><a href="./report-list-summary.php?option=a-2&weekly-date-picker=<?=$weekly_query?>">ทำสัญญาแล้วแต่เบิกจ่ายไม่เสร็จ น้อยกว่าสองล้านบาท</a></li>
                            <li><a href="./report-list-summary.php?option=a-3&weekly-date-picker=<?=$weekly_query?>">ดำเนินการเสร็จแล้วมีเงินเหลือ น้อยกว่าสองล้านบาท</a></li>
                            <li><a href="./report-list-summary.php?option=a-4&weekly-date-picker=<?=$weekly_query?>">ดำเนินการเสร็จไม่มีเงินเหลือ น้อยกว่าสองล้านบาท</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="./report-list-summary.php?option=b-1&weekly-date-picker=<?=$weekly_query?>">รายการที่ยังไม่ทำสัญญา มากกว่าสองล้านบาท</a></li>
                            <li><a href="./report-list-summary.php?option=b-2&weekly-date-picker=<?=$weekly_query?>">ทำสัญญาแล้วแต่เบิกจ่ายไม่เสร็จ มากกว่าสองล้านบาท</a></li>
                            <li><a href="./report-list-summary.php?option=b-3&weekly-date-picker=<?=$weekly_query?>">ดำเนินการเสร็จแล้วมีเงินเหลือ มากกว่าสองล้านบาท</a></li>
                            <li><a href="./report-list-summary.php?option=b-4&weekly-date-picker=<?=$weekly_query?>">ดำเนินการเสร็จไม่มีเงินเหลือ มากกว่าสองล้านบาท</a></li>
                        </ul>
                    </li>
                    <li><a class="navbar-brand" href="reset-data.php">ล้างข้อมูล</a></li>
                    <li><a class="navbar-brand" href="/pull">Update</a></li>
                </ul>
            </div>
          </div>
        </nav>
