<?
    $title = "Home";
    include('./header.php');
?>
        <div class="jumbotron">
          <div class="container">
            <h2>รายการข้อมูลหลัก</h2>
          </div>
        </div>
        <div class="container progress-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        0%
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='container'>
            <div class="row">
                <div class="col-md-6">
                    <form id="main-data-form" action="index_submit.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="year">ปีงบประมาณ</label>
                            <input type="text" class="form-control" id="year" name="year" placeholder="ปีงบประมาณ" required>
                      </div>
                      <input type="file" name="excelFile" value="" placeholder="">
                      <!-- <button type="submit" class="btn btn-default" style="margin-top:20px;">Submit</button> -->
                      <input type="submit" name="submit" value="Submit" style="margin-top:20px;"/>
                    </form>
                </div>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>
        <script type="text/javascript">
            $("#main-data-form").submit(function() {

                // var formData = new FormData($(this)[0]);
                // $.ajax({
                //     url: "index_submit.php",
                //     type: 'POST',
                //     data: formData,
                //     cache: false,
                //     contentType: false,
                //     processData: false,
                // }).done(function(data) {
                //     source.close();
                //     window.location.href = "/list-main-data.php"
                // }).fail(function(data) {
                //     alert(JSON.stringify(data));
                //     source.close();
                // });

                handleProgress();

                // source.onerror = function(e) {
                //   console.log("listen event error: ");
                //   console.log(e);
                // };

                // return false
            })
        </script>
    </body>
</html>
