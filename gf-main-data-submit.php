<?php
require_once('./src/server/reader/GF_List_Main_Reader.php');
if($_FILES['excelFile']['name']) {
    if(!$_FILES['excelFile']['error']) {
        $target_dir = "uploads/";
        $target_file = $target_dir . rawurlencode(basename($_FILES["excelFile"]["name"]));
        if (move_uploaded_file($_FILES['excelFile']['tmp_name'], $target_file)) {
          $reader = new GF_List_Main_Reader;
          $reader->importData($target_file);
          header("Location: gf-main-data-fix.php");
          die();
        } else {
          echo "Possible file upload attack!\n";
        }
    }  else {
        echo 'Ooops!  Your upload triggered the following error:  '.$_FILES['excelFile']['error'];
    }
} else {
    echo "no file upload";
}
