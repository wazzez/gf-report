<?php
require_once('./src/client/fetcher/GF_Main_Data_Fetcher.php');

$action = $_GET['action'];

if ($action == 'fetch') {
	$page = 1;
	$limit = 20;

	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	}

	if (isset($_GET['limit'])) {
		$limit = $_GET['limit'];
	}

	if (isset($_GET['begin-date']) && isset($_GET['end_date']) ) {
	    $beginDate = $_GET['begin-date'];
	    $endDate = $_GET['end_date'];
	} else {
	    $day = date('w');
	    $beginDate = date('Y-m-d', strtotime('-'.$day.' days'));
	    $endDate = date('Y-m-d', strtotime('+'.(6-$day).' days'));
	}

	$fetcher = new GF_Main_Data_Fetcher($beginDate, $endDate);
	$fetcher->displayHTML($page, $limit);
} elseif ($action == 'delete') {
	$day = date('w');
    $beginDate = date('Y-m-d', strtotime('-'.$day.' days'));
    $endDate = date('Y-m-d', strtotime('+'.(6-$day).' days'));
	$fetcher = new GF_Main_Data_Fetcher($beginDate, $endDate);
	$success = $fetcher->deleteData();
	echo $success;
}
