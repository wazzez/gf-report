<?php
include_once('./src/client/fetcher/GF_Main_Date_Fix_Fetcher.php');

$fetcher = new GF_Main_Date_Fix_Fetcher;
if (!empty($_GET['limit'])) {
    $limit = $_GET['limit'];
} else {
    $limit = 20;
}
$title = "แก้ไขรายการข้อมูล GF";
include('./header.php');
?>

<div class="jumbotron">
          <div class="container">
            <h2>แก้ไขรายการข้อมูล GF</h2>
          </div>
        </div>
        <div class="container">
           <form action="gf-main-data-fix-submit.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <input type="submit" class="btn btn-default" name="submit" value="Submit" style="margin-bottom:20px;"/>
                <div>
                    <table class="table table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th>รหัสงบประมาณ</th>
                                <th>พรบ.</th>
                                <th>จัดสรรถือจ่าย</th>
                                <th>ใบสั่งซื้อ (PO) </th>
                                <th>เบิกเอง</th>
                                <th>ยืนยันการแก้ไข</th>
                            </tr>
                        </thead>
                        <tbody id="content">
                            <? $fetcher->displayHTML(1, $limit)?>
                        </tbody>
                    </table>
                </div>
                <input type="submit" class="btn btn-default" name="submit" value="Submit" style="margin-bottom:20px;"/>

                <div class="row">
                    <div class="col-md-offset-5 col-md-1">
                        <select id="page-control" class="form-control">
                          <option value='20' <?php echo $limit==20 ? "selected" : "" ?>>20</option>
                          <option value='50' <?php echo $limit==50 ? "selected" : "" ?>>50</option>
                          <option value='100' <?php echo $limit==100 ? "selected" : "" ?>>100</option>
                          <option value='all' <?php echo $limit=='all' ? "selected" : "" ?>>ทั้งหมด</option>
                        </select>
                    </div>
                </div>
                <div id="page-selection"></div>
            </form>
        </div>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/jquery.bootpag.min.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">
            $(".row-data td input").change(function() {
                var id = $(this).data("id");
                $("#validated-"+id).prop("checked", true);
            });
        </script>
        <script>
        // init bootpag
        $('#page-selection').bootpag({
            total: <? echo $fetcher->numberOfPages($limit); ?>,
            maxVisible: 20,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
        }).on("page", function(event, /* page number here */ num){
             loadpage(num, <? echo $limit; ?>);
        });

        $('#page-control').change(function() {
            var limit = limit = $(this).val();
            window.location.href = "/gf-main-data-fix.php?limit="+limit;
        });

        function loadpage(page, limit) {
            $("#content").load('gf-main-data-fix-ajax.php?page='+page+'&limit='+limit , function() {
                $('#page-selection').bootpag({total:  <? echo $fetcher->numberOfPages($limit); ?>});
            });
        }
    </script>
    </body>
</html>
