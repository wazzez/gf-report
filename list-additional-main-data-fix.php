<?php
include_once('./src/client/fetcher/List_Agency_Fix_Fetcher.php');

$fetcher = new List_Agency_Fix_Fetcher;
if (!empty($_GET['limit'])) {
    $limit = $_GET['limit'];
} else {
    $limit = 20;
}

$title = "แก้ไขรายการข้อมูลหลัก(เพิ่ม)";
include('./header.php');
?>

<div class="jumbotron">
          <div class="container">
            <h2>แก้ไขรายการข้อมูลหลัก(เพิ่ม)</h2>
          </div>
        </div>
        <div class="container">
          <!--<div class="col-md-6">
              <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                  <div class="form-group">
                      <label for="year">ข้อมูลหลัก(เพิ่ม)</label>
                      <input type="file" name="excelFile" value="" placeholder="" required>
                </div>
                <input type="submit" name="submit" value="Submit" style="margin-bottom:20px;"/>
              </form>
          </div> -->
           <form action="list-additional-main-data-fix-submit.php" method="post" accept-charset="utf-8">
                <input type="submit" class="btn btn-default" name="submit" value="Submit" style="margin-bottom:20px;"/>
                <div>
                    <table class="table table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th>รหัสงบประมาณ</th>
                                <th>หน่วยเบิก</th>
                                <th>ยืนยันการแก้ไข</th>
                            </tr>
                        </thead>
                        <tbody id="content">
                            <? $fetcher->displayHTML(1, $limit)?>
                        </tbody>
                    </table>
                </div>
                <input type="submit" class="btn btn-default" name="submit" value="Submit" style="margin-bottom:20px;"/>
                <br/>
                <div class="row">
                    <div class="col-md-offset-5 col-md-1">
                        <select id="page-control" class="form-control">
                          <option value='20' <?php echo $limit==20 ? "selected" : "" ?>>20</option>
                          <option value='50' <?php echo $limit==50 ? "selected" : "" ?>>50</option>
                          <option value='100' <?php echo $limit==100 ? "selected" : "" ?>>100</option>
                          <option value='all' <?php echo $limit=='all' ? "selected" : "" ?>>ทั้งหมด</option>
                        </select>
                    </div>
                </div>
                <div id="page-selection"></div>
            </form>
        </div>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/jquery.bootpag.min.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">
            $(".row-data td input").change(function() {
                var id = $(this).data("id");
                $("#validated-"+id).prop("checked", true);
            });
        </script>
        <script>
        // init bootpag
        $('#page-selection').bootpag({
            total: <? echo $fetcher->numberOfPages($limit); ?>,
            maxVisible: 20,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
        }).on("page", function(event, /* page number here */ num){
             $("#content").load('list-additional-main-data-fix-ajax.php?page='+num+'&limit=<? echo $limit; ?>'); // some ajax content loading...
        });

        $('#page-control').change(function() {
            var limit = limit = $(this).val();
            window.location.href = "/list-additional-main-data-fix.php?limit="+limit;
        });

        function loadpage(page, limit) {
            $("#content").load('list-additional-main-data-fix-ajax.php?page='+page+'&limit='+limit , function() {
                $('#page-selection').bootpag({total:  <? echo $fetcher->numberOfPages($limit); ?>});
            });
        }
    </script>
    </body>
</html>
