<?php
require_once('src/client/fetcher/Summary_List_Report_Fetcher.php');

$action = $_GET['action'];
$option = $_GET['option'];
$date = $_GET['date'];
if(!isset($date)) {
	die('date not exist');
}

$date = urldecode($date);

if(isset($option)) {
    $fetcher = new Summary_List_Report_Fetcher($date, $option);
} else {
    $fetcher = new Summary_List_Report_Fetcher($date);
}

if ($action == 'page') {
    $page = 1;
    $limit = 20;

    if (isset($_GET['page'])) {
    	$page = $_GET['page'];
    }

    if (isset($_GET['limit'])) {
    	$limit = $_GET['limit'];
    }

    $fetcher->displayHTML($page, $limit);
} elseif($action == 'number-of-items') {
    header('Content-Type: application/json');
    $res = array('number-of-items' => $fetcher->numberOfItems());
    echo json_encode($res);
} elseif($action == 'number-of-pages') {
    $limit = 20;
    header('Content-Type: application/json');
    $res = array('number-of-pages' => $fetcher->numberOfPages($limit));
    echo json_encode($res);
}
