<?php
$title = "เพิ่มรายการข้อมูลหลัก";
include('./src/server/database/database.php');
$conn = Database::connect();
$error = null;

if(isset($_POST) && count($_POST) > 0) {
    $year       = escape_string($_POST['year'], $conn);
    $project_id = escape_string($_POST['project_id'], $conn);
    $id         = escape_string($_POST['id'], $conn);
    $name       = escape_string($_POST['name'], $conn);
    $budget     = escape_string($_POST['budget'], $conn);
    $agency     = escape_string($_POST['agency'], $conn);
    $province   = escape_string($_POST['province'], $conn);
    $request_id = escape_string($_POST['request_id'], $conn);
    if(!validate_request_id($conn, $request_id)) {
        $error = "มีรหัสคำขอ $request_id อยู่แล้ว";
    } else {

        $agency_query = "INSERT INTO list_agency_main_data (list_id, agency, year, validated)
                    VALUES('$id', '$agency', '$year', 1)";
        if(!$conn->query($agency_query)) {
            $error = $conn->error;
            die($error);
        }

        $main_query = "INSERT INTO list_main_data (id, name, budget, province, request_id, year, project_id)
                        VALUES('$id', '$name', '$budget', '$province', '$request_id', '$year', '$project_id')";
        if(!$conn->query($main_query)) {
            $error = $conn->error;
            die($error);
        }
        if(isset($_GET['gf_id'])) {
            header("Location: main-data-fix.php");
        } else {
            header("Location: list-main-data.php");
        }

    }
}

if(isset($_GET['gf_id'])) {
    $gfid = $_GET['gf_id'];
    $query = "SELECT id, list_id, budget FROM gf_list_data WHERE id = $gfid limit 1";
    $result = $conn->query($query);
    if ($result) {
        $row = mysqli_fetch_assoc($result);
        $list_id = $row['list_id'];
        $budget  = $row['budget'];
    }
}

function validate_request_id($conn, $request_id) {
    $query = "SELECT COUNT(request_id) AS total FROM list_main_data WHERE request_id = $request_id";
    $result = $conn->query($query);
    if(!$result) {
        die('get request id error '.$conn->error);
    }
    $row = mysqli_fetch_assoc($result);
    return $row['total'] == 0;
}

include('./header.php');
?>

        <div class="jumbotron">
          <div class="container">
            <h2>รายการข้อมูลหลัก</h2>
          </div>
        </div>
        <div class="container">
            <div class="col-md-6">
              <form method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="form-group">
                      <label for="year">ปีงบประมาณ</label>
                      <input type="text" class="form-control" name="year" value="" placeholder="" required>
                </div>
                <div class="form-group">
                      <label for="project_id">ผลผลิตโครงการ</label>
                      <select class="form-control" name="project_id">
                            <?php
                                $query = 'SELECT id, name FROM list_project';
                                $result = $conn->query($query);
                                if(!$result) {
                                    die('get project error'.$conn->error);
                                }
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $project_id = $row['id'];
                                    $project_name = $row['name'];
                                    echo "<option value='$project_id'>$project_name</option>";
                                }
                            ?>
                      </select>
                </div>
                <div class="form-group">
                      <label for="id">รหัสงบประมาณ</label>
                      <input type="text" class="form-control" name="id" value="<? echo $list_id; ?>" placeholder="" required>
                </div>
                <div class="form-group">
                      <label for="name">ชื่อรายการ</label>
                      <input type="text" class="form-control" name="name" value="" placeholder="" required>
                </div>
                <div class="form-group">
                      <label for="budget">จำนวนเงิน</label>
                      <input type="text" class="form-control" name="budget" value="" placeholder="" required>
                </div>
                <div class="form-group">
                      <label for="agency">หน่วยเบิก</label>
                      <input type="text" class="form-control" name="agency" value="" placeholder="" required>
                </div>
                <div class="form-group">
                      <label for="province">จังหวัด</label>
                      <input type="text" class="form-control" name="province" value="" placeholder="" required>
                </div>
                <div class="form-group">
                      <label for="request_id">รหัสคำขอ</label>
                      <input type="text" class="form-control" name="request_id" value="" placeholder="" required>
                </div>
                <input type="submit" class="btn btn-default" name="submit" value="Submit" style="margin-bottom:20px;"/>
              </form>
          </div>
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            <?php
                if($error) {
                    echo "alert('$error');";
                }
            ?>

        </script>
    </body>
</html>
