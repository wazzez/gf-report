<?php
require_once('./src/server/reader/List_Main_Reader.php');

if($_FILES['excelFile']['name'] && isset($_POST['year'])) {
	//if no errors...
	$year = $_POST['year'];
	if(!$_FILES['excelFile']['error']) {
		$target_dir = "uploads/";
		$target_file = $target_dir . basename($_FILES["excelFile"]["name"]);
		if (move_uploaded_file($_FILES['excelFile']['tmp_name'], $target_file)) {
			$reader = new List_Main_Reader;
			$reader->importOrderMainData($target_file, $year);
			header("Location: list-main-data.php");
			die();
		} else {
			http_response_code(400);
			echo json_encode(["error" => "Possible file upload attack!\n"]);
		}
	} else {
		http_response_code(400);
		echo json_encode(["error" => 'Ooops!  Your upload triggered the following error:  '.$_FILES['excelFile']['error']]);
	}
} else {
	http_response_code(400);
	echo json_encode(["error" => "no file upload"]);
}
