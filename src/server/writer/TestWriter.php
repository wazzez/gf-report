<?php

include_once('Writer.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/src/client/fetcher/Summary_List_Report_Fetcher.php');

ini_set('memory_limit', '384M');
ini_set('max_execution_time', 1200);

$headers = array(
		'ผลการเบิกจ่ายเงินงบประมาณ ประจำปีงบประมาณ พ.ศ. 2560',
		'ตั้งแต่ต้นปีงบประมาณ จนถึงวันที่ 1 พฤศจิกายน 2559',
		'สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน'
	);

$titles = array(
	'ลำดับ',
	'รหัสรายการ',
	'รายการ',
	'งบที่ได้รับจัดสรร',
	'ใบสั่งซื้อ (PO) ',
	'เบิกจ่ายทั้งสิ้น ',
	'คงเหลือ',
	'หน่วยเบิก',
	'จังหวัด',
	'วงเงินตาม พ.ร.บ.',
	'รหัสคำขอ'
	);

$data = array();

$fetcher = new Summary_List_Report_Fetcher('2016-11-20 - 2016-11-26', 'a-2');
$sums = $fetcher->fetchSum('a-2');

$sumData = array(
		'รวม',
		null,
		convertNumber($sums['sum_budget']),
		convertNumber($sums['sum_po']),
		convertNumber($sums['sum_disburse']),
		convertNumber($sums['sum_remain'])
	);
array_push($data, $sumData);

$list = $fetcher->fetchData(1, 99999);
$data = array_merge($data, $list);

// header('Content-Type: application/json');
// echo json_encode($data);
// die();

$writer = new Writer($headers, $titles, $data);
$writer->saveFile();
