<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php');

/**
 *
 */
class Writer
{
    var $headers        = array();
    var $titles         = array();
    var $data           = array();
    var $projectSums    = array();

    var $objPHPExcel;
    var $sheetIndex = 0;

    var $currentWritingRow = 0;

    function __construct($headers, $titles, $data, $projectSums, $sheetIndex = 0) {
        $this->headers      = $headers;
        $this->titles       = $titles;
        $this->data         = $data;
        $this->projectSums  = $projectSums;

        $this->objPHPExcel  = new PHPExcel();
        $this->sheetIndex   = $sheetIndex;

        $this->objPHPExcel->setActiveSheetIndex($sheetIndex);
    }

    function __destruct() {
        $this->headers      = null;
        $this->titles       = null;
        $this->data         = null;
        $this->objPHPExcel  = null;
    }

    function filePath() {
        return $_SERVER["DOCUMENT_ROOT"]."/tmp/test.xls";
    }

    /// http://stackoverflow.com/questions/12611148/how-to-export-data-to-an-excel-file-using-phpexcel
    public function downloadFile($fileName = 'report') {
         $this->createFile();
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = new PHPExcel_Writer_Excel2007($this->objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function saveFile() {
        $this->createFile();

        $objWriter = new PHPExcel_Writer_Excel2007($this->objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save($this->filePath());
    }

    private function createFile() {
        $this->createHeaders();
        $this->createTitles();
        $this->createData();
    }

    private function createHeaders() {
        $this->currentWritingRow = 3;

        foreach ($this->headers as $value) {
            $this->objPHPExcel->getActiveSheet()->SetCellValue('B'.$this->currentWritingRow, $value);
            $this->currentWritingRow++;
        }
    }

    private function createTitles() {
        $column = 'B';
        foreach ($this->titles as $value) {
            $this->objPHPExcel->getActiveSheet()->SetCellValue($column.$this->currentWritingRow, $value);
            $column++;
        }
        $this->currentWritingRow++;
    }

    private function createData() {
        $i = 0;
        $lastProjectId = "";
        foreach ($this->data as $data) {
            if ($i > 0 && !empty($this->projectSums) && $lastProjectId != $data['project_id']) {
                $projectID = $data['project_id'];
                $project = $this->projectSums[$projectID];

                $this->objPHPExcel->getActiveSheet()->SetCellValue('B'.$this->currentWritingRow, $projectID . ":".$project['name']);
                $this->objPHPExcel->getActiveSheet()->SetCellValue('C'.$this->currentWritingRow, "รวม");
                $this->objPHPExcel->getActiveSheet()->SetCellValue('E'.$this->currentWritingRow, $project['sum']['sum_budget']);
                $this->objPHPExcel->getActiveSheet()->SetCellValue('F'.$this->currentWritingRow, $project['sum']['sum_po']);
                $this->objPHPExcel->getActiveSheet()->SetCellValue('G'.$this->currentWritingRow, $project['sum']['sum_disburse']);
                $this->objPHPExcel->getActiveSheet()->SetCellValue('H'.$this->currentWritingRow, $project['sum']['sum_remain']);

                $lastProjectId = $projectID;
                $this->currentWritingRow++;
            }
            if ($i > 0) {
                $this->objPHPExcel->getActiveSheet()->SetCellValue('B'.$this->currentWritingRow, $i);
            }

            $column = 'C';
            foreach ($data as $key => $value) {
                if ($key != 'project_id') {
                    $this->objPHPExcel->getActiveSheet()->SetCellValue($column.$this->currentWritingRow, $value);
                    $column++;
                }
            }
            $this->currentWritingRow++;
            $i++;
        }
    }
}
