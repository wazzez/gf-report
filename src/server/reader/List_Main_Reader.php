<?php
include_once('./src/server/reader/Reader.php');
include_once('./src/server/database/database.php');

class List_Main_Reader {
    public function importOrderMainData($filePath, $year) {
        try {
            session_start();
            $_SESSION["progress"] = 0;
            session_write_close();
            $reader = new Reader($filePath);
            $subtotalRows = $reader->subtotalRows();
            $chunkSize = 5120;
            $filter = new ChunkReadFilter();
            $reader->objReader->setReadFilter($filter);
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 12000);
            $conn = Database::connect();
            $year = escape_string($year, $conn);
            $startRow = 4;
            $readCount = 0;
            foreach ($reader->worksheetNames as $index => $sheetName) {
                $endRow = $startRow + $chunkSize;
                while($startRow < 65536) {
                    $filter->setRows($startRow,$chunkSize);
                    $reader->setLoadSheetsOnly($sheetName);
                    $reader->load();
                    $sheet = $reader->sheetFromIndex(0);
                    $sheetData = $sheet->toArray(null,true,true,true);
                    $insertValues = [];
                    $orderProjectData = $sheetData['4']['A'];
                    // explode proejct string
                    $orderProjectArray = explode(' : ', $orderProjectData);
                    if($startRow == 4) {
                        if ($startRow == 4 && count($orderProjectArray) != 2) { die("$sheetName $orderProjectData File format is not correct"); }
                        $orderProjectId     = escape_string($orderProjectArray[0], $conn);
                        $orderProjectName   = escape_string($orderProjectArray[1], $conn);

                        // insert order project data to database
                        $this->insertOrderProject($orderProjectId, $orderProjectName, $conn);
                        // get order main data
                    }

                    $insertCount = 0;
                    for($i=0; $i< $chunkSize; $i++) {
                        $row = $startRow + $i;
                        if($row > count($sheetData)) {
                            error_log("out of rang: $sheetName at $row");
                            $this->startInsertData($insertValues, $conn);
                            $insertValues = null;
                            $sheetData = null;
                            $reader->unload();
                            $startRow = 4;
                            break 2;
                        }

                        $id         = escape_string($sheetData[$row]['B'], $conn);
                        $name       = escape_string($sheetData[$row]['C'], $conn);
                        $budget     = escape_string($sheetData[$row]['D'], $conn);
                        $province   = escape_string($sheetData[$row]['E'], $conn);
                        $requestId  = escape_string($sheetData[$row]['F'], $conn);


                        if(empty($id) || empty($name) || empty($budget) || empty($province) || empty($requestId)) {
                            continue;
                        }

                        if ($insertCount >= 200) {
                            $this->startInsertData($insertValues, $conn);
                            $insertValues = [];
                        }
                        $insertValues[] = $this->insertValue($orderProjectId, $id, $name, $budget, $province, $requestId, $year);
                        $insertCount++;
                        $readCount++;
                        header_remove('Set-Cookie');
                        session_start();
                        $_SESSION["progress"] = $readCount / $subtotalRows;
                        session_write_close();
                    }
                    $this->startInsertData($insertValues, $conn);
                    $insertValues = null;
                    $sheetData = null;
                    $reader->unload();
                    $startRow += $chunkSize;
                }
            }
            Database::disconnect($conn);
            header_remove('Set-Cookie');
            session_start();
            $_SESSION["progress"] = 1;
            session_write_close();
        } catch(PHPExcel_Reader_Exception $e) {
            die('Error loading file: '.$e->getMessage());
        }
    }

    function insertOrderProject($id, $name, $conn) {
        $query = 'INSERT INTO list_project (id, name)
                    VALUES("'.$id.'", "'.$name.'") ON DUPLICATE KEY UPDATE name="'.$name.'"';
        $result = $conn->query($query);
        return $result;
    }

    function insertValue($projectId, $id, $name, $budget, $province, $requestId, $year) {
        return "('$id', '$name', $budget, '$province', $requestId, $year, '$projectId')";
    }



    function startInsertData($values, $conn) {
        $valuesString = implode(",", $values);
        $query = "REPLACE INTO list_main_data (id, name, budget, province, request_id, year, project_id) VALUES $valuesString";
        $result = $conn->query($query);
        if (!$result) {
            die("insert data error:".$conn->error);
        }
        return $result;
    }
}
