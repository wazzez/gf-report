<?php
session_start();
include_once('./src/server/reader/Reader.php');
include_once('./src/server/database/database.php');

class List_Main_Additional_Area_Reader {
    public function importData($filePath) {
        try {
            session_start();
            $_SESSION["progress"] = 0;
            session_write_close();
            $reader = new Reader($filePath);
            $subtotalRows = $reader->subtotalRows();
            $chunkSize = 5120;
            $filter = new ChunkReadFilter();
            $reader->objReader->setReadFilter($filter);
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 12000);
            $conn = Database::connect();
            $startRow = 2;
            $readCount = 0;
            foreach ($reader->worksheetNames as $index => $sheetName) {
                while($startRow < 65536) {
                    $filter->setRows($startRow,$chunkSize);
                    $reader->setLoadSheetsOnly($sheetName);
                    $reader->load();
                    $sheet = $reader->sheetFromIndex(0);
                    $sheetData = $sheet->toArray(null,true,true,true);
                    $insertValues = [];

                    for($i=0; $i< $chunkSize; $i++) {
                        $row = $startRow + $i;
                        if($row > count($sheetData)) {
                            error_log("out of rang: $sheetName at $row");
                            $this->startInsertData($insertValues, $conn);
                            $insertValues = null;
                            $sheetData = null;
                            $reader->unload();
                            $startRow = 2;
                            break 2;
                        }

                        $id                 = escape_string($sheetData[$row]['C'], $conn);
                        $province           = escape_string($sheetData[$row]['A'], $conn);
                        $area_unit_name     = escape_string($sheetData[$row]['B'], $conn);

                        if(empty($id) || empty($province) || empty($area_unit_name)) {
                            continue;
                        }

                        $this->updateValue($id, $area_unit_name, $conn);
                        $readCount++;
                        header_remove('Set-Cookie');
                        session_start();
                        $_SESSION["progress"] = $readCount / $subtotalRows;
                        session_write_close();
                    }
                    $insertValues = null;
                    $sheetData = null;
                    $reader->unload();
                    $startRow += $chunkSize;
                }
            }
            
            Database::disconnect($conn);
            header_remove('Set-Cookie');
            session_start();
            $_SESSION["progress"] = 1;
            session_write_close();
        } catch(PHPExcel_Reader_Exception $e) {
            die('Error loading file: '.$e->getMessage());
        }
    }

    function updateValue($id, $name, $conn) {
        $query = "UPDATE list_agency_main_data SET agency='$name'
                    WHERE agency_id='$id'";
        if(!$conn->query($query)) {
            echo "$query <br/>";
            die($conn->error);
        }
    }

    function insertValue($id, $province, $area_unit_name) {
        return "('$id', '$province', '$area_unit_name')";
    }

    function startInsertData($values, $conn) {
        $valuesString = implode(",", $values);
        $query = "INSERT INTO list_agency_main_data (id, name, budget, province, request_id, year, project_id) VALUES $valuesString";
        $result = $conn->query($query);
        return $result;
    }
}
