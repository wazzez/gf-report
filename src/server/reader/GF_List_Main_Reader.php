<?php

include_once('./src/server/reader/Reader.php');
include_once('./src/server/database/database.php');


class GF_LIST_Model{
    var $id;
    var $name;
    var $limit;
    var $badget;
    var $po;
    var $disburse;

    var $count;
    function __construct($id, $name, $limit, $budget, $po, $disburse, $count) {
        $this->id       = $id;
        $this->name     = $name;
        $this->limit    = $limit;
        $this->budget   = doubleval($budget);
        $this->po       = doubleval($po);
        $this->disburse = doubleval($disburse);
        $this->count    = $count;
    }
}


class GF_List_Main_Reader {
    public function importData($filePath) {
        try {
            session_start();
            $_SESSION["progress"] = 0;
            session_write_close();
            $conn = Database::connect();
            $reader = new Reader($filePath);
            $subtotalRows = $reader->subtotalRows();
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 12000);
            $chunkSize = 2650;
            $filter = new ChunkReadFilter();
            $reader->objReader->setReadFilter($filter);
            $lastAgency = null;
            $listPool = array();
            $count = 1;
            $startRow = 3;
            $readCount = 0;
            foreach ($reader->worksheetNames as $index => $sheetName) {
                while($startRow < 65536) {
                    $filter->setRows($startRow,$chunkSize);
                    $reader->setLoadSheetsOnly($sheetName);
                    $reader->load();
                    $sheet = $reader->sheetFromIndex(0);
                    $sheetData = $sheet->toArray(null,true,true,true);
                    $isBegin = false;

                    for($i=0; $i< $chunkSize; $i++) {
                        $row = $startRow + $i;

                        //end of sheet
                        if($row > count($sheetData)) {
                            $this->insertData($listPool, $isBegin, $conn);
                            unset($listPool);
                            $listPool = array();
                            $isBegin = true;
                            $count = 1;
                            $lastAgency = null;
                            $startRow = 3;
                            break 2;
                        }


                        if($this->isBegin($sheetData, $row)) {
                            $this->insertData($listPool, $isBegin, $conn);
                            unset($listPool);
                            $listPool = array();
                            $isBegin = true;
                            $count = 1;
                            $lastAgency = null;
                        } elseif($this->isGF($sheetData, $row)) {
                            $id         = escape_string($sheetData[$row]['E'], $conn);
                            $name       = escape_string($sheetData[$row]['F'], $conn);
                            $limit      = escape_string($sheetData[$row]['G'], $conn);
                            $budget     = escape_string($sheetData[$row]['H'], $conn);
                            $po         = escape_string($sheetData[$row]['I'], $conn);
                            $disburse   = escape_string($sheetData[$row]['J'], $conn);

                            if($lastAgency == null) {
                                $lastAgency = new GF_LIST_Model($id, $name, $limit, $budget, $po, $disburse, $count);
                                array_push($listPool, $lastAgency);
                            } elseif($lastAgency->id == $id) {
                                $count++;
                                $lastAgency = new GF_LIST_Model($id, $name, $limit, $budget, $po, $disburse, $count);
                                array_push($listPool, $lastAgency);
                            } else {
                                $this->insertData($listPool, $isBegin, $conn);
                                $isBegin = false;
                                unset($listPool);
                                $listPool = array();
                                $count = 0;
                                $lastAgency = new GF_LIST_Model($id, $name, $limit, $budget, $po, $disburse, $count);
                                array_push($listPool, $lastAgency);
                            }
                        } else {
                            //error_log('input gf data read row '.$row.' error');
                        }
                        $readCount++;
                        header_remove('Set-Cookie');
                        session_start();
                        $_SESSION["progress"] = $readCount / $subtotalRows;
                        session_write_close();
                    }
                    $sheetData = null;
                    $reader->unload();
                    $startRow += $chunkSize;
                }
            }
            Database::disconnect($conn);
            header_remove('Set-Cookie');
            session_start();
            $_SESSION["progress"] = 1;
            session_write_close();
        } catch(PHPExcel_Reader_Exception $e) {
            die('Error loading file: '.$e->getMessage());
        }
    }

    private function isBegin($sheetData, $row) {
        $C = $sheetData[$row]['C'];
        $D = $sheetData[$row]['D'];

        return isset($C) && isset($D);
    }

    private function isGF($sheetData, $row) {
        $C = $sheetData[$row]['C'];
        $E = $sheetData[$row]['E'];
        $F = $sheetData[$row]['F'];

        return !isset($C) && isset($E) && isset($F);
    }

    private function insertData($agencies, $isBegin, $conn) {
        if (empty($agencies)) {
            return;
        }
        if(count($agencies) > 10) {
            echo "out of bounds.";
            die(var_dump($agencies));
        }
        $validCount = $isBegin ? 9 : 10;
        $validated = count($agencies) == $validCount;
        if(count($agencies) == 1) {
            $id = $agencies[0]->id;
            $valid = ctype_digit($id) === false;
            $this->insert($agencies, $valid, $conn);
        } else {
            $insertingData = $agencies;
            if($validated) {
                $insertingData = $this->manipulateModels($agencies);
            }
            $this->insert($insertingData, $validated, $conn);
        }
    }

    private function insert(array $models, $validated, $conn) {
        if(!isset($models) || count($models) == 0) {
            return;
        }

        $values = [];
        foreach ($models as $model) {
            $values[] = '("'.$model->id.'",
                    "'.$model->name.'",
                    "'.$model->limit.'",
                    "'.$model->budget.'",
                    "'.$model->po.'",
                    "'.$model->disburse.'",
                    "'.$validated.'")';
        }
        $valuesString = implode(",", $values);
        $query = 'INSERT INTO gf_list_data (list_id, list_name, financial_amount, budget, po, disburse, validated)
                    VALUES '.$valuesString;
        if(!$conn->query($query)) {
            echo "$query<br/>";
            die($conn->error);
        } else {

        }
    }

    private function manipulateModels($models) {
        $data = array();
        foreach($models as $model) {
            $id = $model->id;
            $id = substr($id,0,-1);
            $id = $id . $model->count;
            $model->id = $id;
            array_push($data, $model);
        }
        return $data;
    }

}
