<?php

include_once('./src/server/reader/Reader.php');
include_once('./src/server/database/database.php');
header('Cache-Control: no-cache');
header("Pragma: no-cache");

class Agency_Model{
    var $id;
    var $name;
    var $agencies = array();
    var $funded_program_ids = [];
    var $count;
    function __construct($id, $name, $count) {
        $this->id       = $id;
        $this->name     = $name;
        $this->agencies = array();
        $this->funded_program_ids = [];
        $this->count    = $count;
    }
}


class List_Main_Additional_Reader {
    public function importData($filePath) {
        try {
            session_start();
            $_SESSION["progress"] = 0;
            session_write_close();
            $conn = Database::connect();
            $reader = new Reader($filePath);
            $subtotalRows = $reader->subtotalRows();
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 12000);
            $chunkSize = 5120;
            $filter = new ChunkReadFilter();
            $reader->objReader->setReadFilter($filter);
            $lastAgency = null;
            $listPool = array();
            $count = 1;
            $startRow = 3;
            $readCount = 0;

            $this->truncateTable($conn);

            foreach ($reader->worksheetNames as $index => $sheetName) {
                while($startRow < 65536) {
                    $filter->setRows($startRow,$chunkSize);
                    $reader->setLoadSheetsOnly($sheetName);
                    $reader->load();
                    $sheet = $reader->sheetFromIndex(0);
                    $sheetData = $sheet->toArray(null,true,true,true);
                    $isBegin = false;

                    for($i=0; $i< $chunkSize; $i++) {
                        $row = $startRow + $i;

                        // insert data if end of sheet
                        if($row > count($sheetData)) {
                            $this->insertData($listPool, $isBegin, $conn);
                            unset($listPool);
                            $listPool = array();
                            $isBegin = true;
                            $count = 1;
                            $lastAgency = null;
                            $startRow = 3;
                            break 2;
                        }
                        if($this->isBegin($sheetData, $row)) {
                            $this->insertData($listPool, $isBegin, $conn);
                            unset($listPool);
                            $listPool = array();
                            $isBegin = true;
                            $count = 1;
                            $lastAgency = null;
                        }
                        elseif($this->isListIdRow($sheetData, $row)) {
                            $id = $sheetData[$row]['E'];
                            $name = $sheetData[$row]['F'];
                            if($lastAgency == null) {
                                $lastAgency = new Agency_Model($id, $name, $count);
                                array_push($listPool, $lastAgency);
                            } elseif($lastAgency->id == $id) {
                                $count++;
                                $lastAgency = new Agency_Model($id, $name, $count);
                                array_push($listPool, $lastAgency);
                            } else {
                                $this->insertData($listPool, $isBegin, $conn);
                                $isBegin = false;
                                unset($listPool);
                                $listPool = array();
                                $count = 0;
                                $lastAgency = new Agency_Model($id, $name, $count);
                                array_push($listPool, $lastAgency);
                            }
                        }
                        elseif($this->isAgencyRow($sheetData, $row)) {
                            $agency = escape_string($sheetData[$row]['H'], $conn);
                            $funded_id = escape_string($sheetData[$row]['G'], $conn);
                            array_push($lastAgency->funded_program_ids, $funded_id);
                            array_push($lastAgency->agencies, $agency);
                        }

                        $readCount++;
                        header_remove('Set-Cookie');
                        session_start();
                        $_SESSION["progress"] = $readCount / $subtotalRows;
                        session_write_close();
                    }
                    $reader->unload();
                    $startRow += $chunkSize;
                }
            }
            Database::disconnect($conn);
            header_remove('Set-Cookie');
            session_start();
            $_SESSION["progress"] = 1;
            session_write_close();
        } catch(PHPExcel_Reader_Exception $e) {
            die('Error loading file: '.$e->getMessage());
        }
    }

    private function isBegin($sheetData, $row) {
        $C = $sheetData[$row]['C'];
        $D = $sheetData[$row]['D'];
        $E = $sheetData[$row]['E'];

        return isset($C) && isset($D) && isset($E);
    }

    private function isListIdRow($sheetData, $row) {
        $E = $sheetData[$row]['E'];
        $F = $sheetData[$row]['F'];

        return isset($E) && isset($F);
    }

    private function isAgencyRow($sheetData, $row) {
        $E = $sheetData[$row]['E'];
        $H = $sheetData[$row]['H'];

        return !isset($E) && isset($H);
    }

    private function insertData($agencies, $isBegin, $conn) {
        if(count($agencies) > 10) {
            die(var_dump($agencies));
        }
        $validCount = $isBegin ? 9 : 10;
        $validated = count($agencies) == $validCount;

        if(count($agencies) == 1) {
            $agency = $agencies[0];
            $valid  = !ctype_digit($agency->id);
            $this->insertAgency($agency, $valid, $conn);
        } else {
            $insertingData = $agencies;
            if($validated) {
                $insertingData = $this->manipulateAgencies($agencies);
            }
            // die(var_dump($insertingData));
            foreach($insertingData as $agency) {
                $this->insertAgency($agency, $validated, $conn);
            }
        }
    }

    private function insertAgency($agency, $validated, $conn) {
        $i = 0;
        foreach($agency->agencies as $title) {
            $funded_id = $agency->funded_program_ids[$i];
            $query = 'INSERT INTO list_agency_main_data (list_id, list_name, agency, agency_id, validated)
                    VALUES("'.$agency->id.'", "'.$agency->name.'", "'.$title.'","'.$funded_id.'" ,"'.$validated.'")';
            if(!$conn->query($query)) {
                echo "$query <br/>";
                die($conn->error);
            }

        }
    }

    private function manipulateAgencies($agencies) {
        $data = array();
        foreach($agencies as $agency) {
            $id = $agency->id;
            $id = substr($id,0,-1);
            $id = $id . $agency->count;
            $agency->id = $id;
            array_push($data, $agency);
        }
        return $data;
    }

    private function truncateTable($conn) {
        $query = 'TRUNCATE `list_agency_main_data`';
        if(!$conn->query($query)) {
            echo "$query <br/>";
            die($conn->error);
        }
    }

}
