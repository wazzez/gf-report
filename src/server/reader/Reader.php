<?php
require_once('./vendor/autoload.php');

class Reader {
    private $filePath;

    var $fileType;
    var $worksheetNames;
    var $objReader;
    var $objPHPExcel;

    public function __construct($filePath) {
        $this->filePath = $filePath;

        try {
            /**  Identify the type of $inputFileName  **/
            $this->fileType = PHPExcel_IOFactory::identify($filePath);
            /**  Create a new Reader of the type that has been identified  **/
            $this->objReader = PHPExcel_IOFactory::createReader($this->fileType);
            $this->worksheetNames = $this->objReader->listWorksheetNames($filePath);
            /**  Advise the Reader that we only want to load cell data  **/
            $this->objReader->setReadDataOnly(true);
        } catch(PHPExcel_Reader_Exception $e) {
            throw $e;
        }
    }

    function __destruct() {
        $this->unload();
        unset($this->objReader);
        unset($this->fileType);
        unset($this->worksheetNames);
        unset($this->filePath);
   }

    public function load() {
        $this->objPHPExcel = $this->objReader->load($this->filePath);
    }

    public function unload() {
        if($this->objPHPExcel) {
            $this->objPHPExcel->disconnectWorksheets();
            $this->objPHPExcel = null;
        }
    }

    public function setLoadSheetsOnly($worksheetName) {
        $this->objReader->setLoadSheetsOnly($worksheetName);
    }

    public function activeSheet($index) {
        $this->objPHPExcel->setActiveSheetIndex($index);
    }

    // getter
    public function getActiveSheet() {
        $this->objPHPExcel->getActiveSheet();
    }

    public function sheetFromIndex($index) {
        return $this->objPHPExcel->getSheet($index);
    }

    public function totalRow($index) {
        $sheet = $this->sheetFromIndex($index);
        return $sheet->getHighestRow();
    }

    public function totalColumn($index) {
        $sheet = $this->sheetFromIndex($index);
        return $sheet->getHighestColumn();
    }

    public function beginningRow() {
        return 6;
    }

    public function headers($sheet) {
        $headers = array(
                "id",
                "name",
                "budget",
                "province",
                "requestId"
            );
        return $headers;
    }

    public function beginRead() {
        try {
            /**  Load $inputFileName to a PHPExcel Object  **/

        } catch(PHPExcel_Reader_Exception $e) {
            throw $e;
        }
    }

    public function subtotalRows() {
        $objReader     = PHPExcel_IOFactory::createReader($this->fileType);
        $worksheetData = $objReader->listWorksheetInfo($this->filePath);
        $count = 0;
        foreach ($worksheetData as $w) {
            $count += $w['totalRows'];
        }
        return $count;
    }

    // loop sheets
    // get height row and column
    // declar data's begining row
    // declar headers array
    // begin read data row by row
        // insert to database
}


/** Define a Read Filter class implementing PHPExcel_Reader_IReadFilter **/
class ChunkReadFilter implements PHPExcel_Reader_IReadFilter
{
    private $_startRow = 0;
    private $_endRow   = 0;
    /** Set the list of rows that we want to read */
    public function setRows($startRow, $chunkSize) {
        $this->_startRow = $startRow;
        $this->_endRow = $startRow + $chunkSize;
    }
    public function readCell($column, $row, $worksheetName = '') {
    // Only read the heading row, and the configured rows
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}

class MyReadFilter implements PHPExcel_Reader_IReadFilter
{
    private $_startRow = 0;
    private $_endRow   = 0;
    private $_columns  = array();
    /** Get the list of rows and columns to read */
    public function __construct($startRow, $endRow, $columns) {
        $this->_startRow = $startRow;
        $this->_endRow = $endRow;
        $this->_columns = $columns;
    }

    public function setRows($startRow, $endRow) {
        $this->_startRow = $startRow;
        $this->_endRow = $endRow;
    }

    public function setColumns($columns) {
        $this->_columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = '') { // Only read the rows and columns that were configured
        if ($row >= $this->_startRow && $row <= $this->_endRow) {
            if (in_array($column,$this->_columns)) {
                return true;
            }
        }
            return false;
    }
}
