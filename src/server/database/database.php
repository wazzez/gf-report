<?php

include_once('db_config.php');

/**
 *
 */
class Database {

    public static function connect() {
        $conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        mysqli_set_charset($conn, "utf8");
        return $conn;
    }

    public static function disconnect($conn) {
        mysqli_close($conn);
    }
}

function escape_string($s, $conn = null) {
    if ($conn == null) {
        $conn = Database::connect();
    }
    $es = mysqli_real_escape_string($conn, $s);
    return $es;
}
