<?php
include_once('Fetcher.php');
/**
 *
 */
class GF_Main_Data_Fetcher extends Fetcher {
    var $beginDate;
    var $endDate;

    public function __construct($beginDate, $endDate) {
        $this->beginDate = $beginDate;
        $this->endDate = $endDate;
    }

    public function queryNumberOfItems() {
        return "SELECT count(id) as total from gf_list_data WHERE submited_date BETWEEN '$this->beginDate' AND '$this->endDate'";
    }

    function queryFetchData($page, $limit) {
        $from = ($page-1) * $limit;
        $query = "SELECT list_id, financial_amount, budget, po, disburse , submited_date
                    FROM gf_list_data
                    WHERE submited_date BETWEEN '$this->beginDate' AND '$this->endDate'
                    ORDER BY 'id'
                    LIMIT $from, $limit";
        return $query;
    }

    public function displayHTML($page, $limit) {
        $items = $this->fetchData($page, $limit);
        foreach ($items as $item) {
            echo "<tr>".PHP_EOL;
            foreach ($item as $key => $value) {
                echo "<td>$value</td>";
            }
            echo "</tr>".PHP_EOL;
        }
    }

    public function deleteData() {
        $conn = Database::connect();
        $query = "DELETE FROM gf_list_data
                    WHERE submited_date BETWEEN '$this->beginDate' AND '$this->endDate'";
        if (!$conn->query($query)) {
            die("query($query) error ".$conn->error);
        }
        return true;
    }
}
