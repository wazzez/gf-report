<?php
include_once($_SERVER["DOCUMENT_ROOT"].'/src/server/database/database.php');

class Fetcher {
    public function numberOfPages($limit) {
        if ($limit == 'all') {
            $limit = 999999;
        }
        $numberOfPages = ceil($this->numberOfItems()/$limit);
        return $numberOfPages;
    }

    function queryNumberOfItems() {
        return '';
    }

    public function numberOfItems() {
        $conn = Database::connect();
        $query = $this->queryNumberOfItems();
        $result = $conn->query($query);
        $data = mysqli_fetch_assoc($result);
        $total = $data['total'];
        return $total;
    }

    function queryFetchData($page, $limit) {
        $query = '';
        return $query;
    }

    public function fetchData($page, $limit) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 12000);
        if ($limit == 'all') {
            $limit = 999999;
        }
        $query = $this->queryFetchData($page, $limit);

        $conn = Database::connect();
        $result = $conn->query($query);
        if(!$result) {
            die("query($query) error ".$conn->error);
        }

        $items = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($items, $row);
        }
        mysqli_free_result($result);
        return $items;
    }

    public function printTableBody($page, $limit) {
        $items = $this->fetchData($page, $limit);
        foreach ($items as $item) {
            echo "<tr>".PHP_EOL;
            foreach ($item as $key => $value) {
                echo "<td>$value</td>";
            }
            echo "</tr>".PHP_EOL;
        }
    }
}
