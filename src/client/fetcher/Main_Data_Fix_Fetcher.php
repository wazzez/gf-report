<?php
include_once('Fetcher.php');
/**
 *
 */
class Main_Data_Fix_Fetcher extends Fetcher {
    public function queryNumberOfItems() {
        return 'SELECT count(g.id) as total
                FROM gf_list_data g
                LEFT JOIN list_main_data m ON g.list_id = m.id WHERE m.id IS NULL';
    }

    function queryFetchData($page, $limit) {
        $from = ($page-1) * $limit;
        $query = 'SELECT g.*
                  FROM gf_list_data g
                  LEFT JOIN list_main_data m ON g.list_id = m.id WHERE m.id IS NULL
                  ORDER BY "g.list_id"
                  LIMIT '.$from.', '.$limit;
        return $query;
    }

    public function displayHTML($page, $limit) {
        $items = $this->fetchData($page, $limit);
        $year = $this->year();
        foreach ($items as $item) {
            $list_id = $item['list_id'];
            $budget = $item['budget'];
            echo "<tr class='row-data' data-edited='0' id='row-$list_id'>".PHP_EOL;
            echo "<td>$list_id</td>".PHP_EOL;
            echo "<td>".PHP_EOL.$this->projectOptionsHTML($list_id)."</td>".PHP_EOL;
            echo "<td> <input type='text' class='form-control' name='name-$list_id' value='".$item['list_name']."' data-id='$list_id'> </td>".PHP_EOL;
            echo "<td> <input type='text' class='form-control' name='budget-$list_id' value='$budget' data-id='$list_id'> </td>".PHP_EOL;
            echo "<td> <input type='text' class='form-control' name='agency_id-$list_id' value='' data-id='$list_id'> </td>".PHP_EOL;
            echo "<td> <input type='text' class='form-control' name='agency-$list_id' value='' data-id='$list_id'> </td>".PHP_EOL;
            echo "<td> <input type='number' class='form-control' name='year-$list_id' value='$year' data-id='$list_id'></td>".PHP_EOL;
            echo "<td> <input type='number' class='form-control' name='request_id-$list_id' value='' data-id='$list_id'></td>".PHP_EOL;
            echo "<td> <input type='checkbox' id='validated-$list_id' name='validated-$list_id' tabindex='-1'></td>".PHP_EOL;

            // foreach ($item as $key => $value) {
            //     echo "<td>$value</td>";
            // }

            // echo "<td><a href='list-main-data-add.php?gf_id=".$item['id']."'</a>เพิ่ม</td>";
            echo "</tr>".PHP_EOL;
        }
    }

    private function projectOptionsHTML($id) {
        $conn = Database::connect();
        $query = 'SELECT * from `list_project`';
        $result = $conn->query($query);
        if(!$result) {
            die("query($query) error ".$conn->error);
        }

        $html = '<select class="form-control" name="project-'.$id.'">'.PHP_EOL;
        while ($row = mysqli_fetch_assoc($result)) {
            $html .= '<option value="'.$row['id'].'">'.$row['name'].'</option>'.PHP_EOL;
        }

        $html .= '</select>'.PHP_EOL;
        mysqli_free_result($result);
        return $html;
    }

    private function year() {
        $month  = intval(date('m'));
        $year   = intval(date('Y'));
        $year   = $month > 10 ? $year + 544 : $year + 543;
        return $year;
    }
}
