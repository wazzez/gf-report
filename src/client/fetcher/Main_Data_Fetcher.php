<?php
include_once('Fetcher.php');
/**
 * 
 */
class Main_Data_Fetcher extends Fetcher {
    public function queryNumberOfItems() {
        return 'SELECT count(id) as total from list_main_data';
    }

    function queryFetchData($page, $limit) {
        $from = ($page-1) * $limit;
        $query = 'SELECT m.id, m.name, m.budget, a.agency, m.province, m.request_id, m.project_id 
                    FROM list_main_data m
                    LEFT JOIN list_agency_main_data a
                        ON m.id = a.list_id
                    ORDER BY "m.id"
                    LIMIT '.$from.', '.$limit;
        return $query;
    }

    public function displayHTML($page, $limit) {
        $items = $this->fetchData($page, $limit);
        foreach ($items as $item) {
            echo "<tr>".PHP_EOL;
            foreach ($item as $key => $value) {
                echo "<td>$value</td>";
            }
            echo "</tr>".PHP_EOL;
        }
    }
}
