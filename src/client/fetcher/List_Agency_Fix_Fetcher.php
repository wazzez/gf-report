<?php
include_once('Fetcher.php');
/**
 *
 */
class List_Agency_Fix_Fetcher extends Fetcher {
    public function queryNumberOfItems() {
        return 'SELECT count(a.list_id) AS total
                FROM list_agency_main_data a
                INNER JOIN (
                        SELECT list_id
                        FROM list_agency_main_data
                        WHERE validated = false
                        GROUP BY list_id
                    ) b
                    ON a.list_id = b.list_id';
    }

    function queryFetchData($page, $limit) {
        $from = ($page-1) * $limit;
        $query = 'SELECT a.id, a.list_id, a.agency, a.validated
                    FROM list_agency_main_data a
                    INNER JOIN (
                        SELECT list_id
                        FROM list_agency_main_data
                        WHERE validated = false
                        GROUP BY list_id
                    ) b
                    ON a.list_id = b.list_id
                    ORDER BY id
                    LIMIT '.$from.', '.$limit;
        // $query = 'SELECT id, list_id, agency
        //             FROM list_agency_main_data
        //             WHERE validated = false
        //             ORDER BY "id"
        //             LIMIT '.$from.', '.$limit;
        return $query;
    }

    public function displayHTML($page, $limit) {
        $items = $this->fetchData($page, $limit);
        foreach ($items as $item) {
            $check = "";
            if (isset($item['validated']) && $item['validated'] === true) {
                $check = " checked";
            }
            echo "<tr class='row-data' data-edited='0' id='row-".$item['id']."'>".PHP_EOL;
            echo "<td>
                        <input type='text' data-id='".$item['id']."' class='form-control' value='".$item['list_id']."' name='lid-".$item['id']."'>
                    </td>".PHP_EOL;
            echo "<td>
                <label>".$item['agency']."</label>
                    </td>".PHP_EOL;
            echo "<td>
                    <input type='checkbox' id='validated-".$item['id']."' name='validated-".$item['id']."'$check tabindex='-1'>
                 </td>";
            echo "</tr>".PHP_EOL;
        }
    }
}
