<?php
include_once('Fetcher.php');
/**
 *
 */
class Main_Data_Agency_Fix_Fetcher extends Fetcher {

    public function queryNumberOfItems() {
        return 'SELECT count(a.list_id) AS total
            	FROM (
            		SELECT a.list_id
            		FROM `list_agency_main_data` a
                  	LEFT JOIN `list_main_data` m
                    	ON a.list_id = m.id
            		WHERE m.id IS NULL
            		GROUP BY a.list_id
                  ) AS a';
    }

    function queryFetchData($page, $limit) {
        $from = ($page-1) * $limit;
        $query = 'SELECT a.*
                FROM `list_agency_main_data` a
                LEFT JOIN `list_main_data` m
                    ON a.list_id = m.id
                WHERE m.id IS NULL
                GROUP BY list_id
                ORDER BY a.list_id
                LIMIT '.$from.', '.$limit;
        return $query;
    }

    public function displayHTML($page, $limit) {
        $items = $this->fetchData($page, $limit);
        $year = $this->year();
        foreach ($items as $item) {
            echo "<tr class='row-data' data-edited='0' id='row-".$item['list_id']."'>".PHP_EOL;

            echo "<td>".$item['list_id']."</td>".PHP_EOL;
            echo "<td>".PHP_EOL.$this->projectOptionsHTML($item['list_id'])."</td>".PHP_EOL;
            echo "<td> <input type='text' class='form-control' name='name-".$item['list_id']."' value='".$item['list_name']."' data-id='".$item['list_id']."'> </td>".PHP_EOL;
            echo "<td> <input type='text' class='form-control' name='budget-".$item['list_id']."' value='' data-id='".$item['list_id']."'> </td>".PHP_EOL;
            echo "<td>".$item['agency_id']."</td>".PHP_EOL;
            echo "<td>".$item['agency']."</td>".PHP_EOL;
            echo "<td> <input type='number' class='form-control' name='year-".$item['list_id']."' value='$year' data-id='".$item['list_id']."'></td>".PHP_EOL;
            echo "<td> <input type='number' class='form-control' name='request_id-".$item['list_id']."' value='' data-id='".$item['list_id']."'></td>".PHP_EOL;
            echo "<td> <input type='checkbox' id='validated-".$item['list_id']."' name='validated-".$item['list_id']."' tabindex='-1'></td>".PHP_EOL;

            echo "</tr>".PHP_EOL;
        }
    }

    private function projectOptionsHTML($id) {
        $conn = Database::connect();
        $query = 'SELECT * from `list_project`';
        $result = $conn->query($query);
        if(!$result) {
            die("query($query) error ".$conn->error);
        }

        $html = '<select class="form-control" name="project-'.$id.'">'.PHP_EOL;
        while ($row = mysqli_fetch_assoc($result)) {
            $html .= '<option value="'.$row['id'].'">'.$row['name'].'</option>'.PHP_EOL;
        }

        $html .= '</select>'.PHP_EOL;
        mysqli_free_result($result);
        return $html;
    }

    private function year() {
        $month  = intval(date('m'));
        $year   = intval(date('Y'));
        $year   = $month > 10 ? $year + 544 : $year + 543;
        return $year;
    }
}
