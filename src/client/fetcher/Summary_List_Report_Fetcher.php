<?php
include_once('Fetcher.php');
/**
 *
 */

function convertNumber($num) {
    return number_format($num, 2, '.', ',');
}
class Summary_List_Report_Fetcher extends Fetcher {
    var $date;
    private $beginDate;
    private $endDate;
    private $option;

    function __construct($date, $option = 'summary') {
        $this->option = $option;
        $this->setDate($date);
    }

    public function setDate($date) {
         $this->date = $date;
         $dates = $this->explodeDate($date);
         $this->beginDate = $dates[0];
         $this->endDate = $dates[1];
    }

    public function explodeDate($date) {
        $dates = explode(' - ', $date);
        if(count($dates) != 2) {
            die('input date format incorrect.');
        }
        return $dates;
    }

    public function queryNumberOfItems() {
        if(empty($this->beginDate) && empty($this->endDate)) {
            die('cannt find begin and end date');
        }

        switch($this->option) {
            case 'summary':
                $query = $this->count_summary_query($this->beginDate, $this->endDate);
                break;
            case 'remain':
                $query = $this->count_still_remain_query($this->beginDate, $this->endDate);
                break;
            case 'a-1':
                $query = $this->count_a_not_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'a-2':
                $query = $this->count_a_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'a-3':
                $query = $this->count_a_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            case 'a-4':
                $query = $this->count_a_no_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            case 'b-1':
                $query = $this->count_b_not_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'b-2':
                $query = $this->count_b_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'b-3':
                $query = $this->count_b_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            case 'b-4':
                $query = $this->count_b_no_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            default:
                $query = $this->count_summary_query($this->beginDate, $this->endDate);
                break;
        }
        return $query;
    }

    public function querySubtotal($projectId = 'all') {
        if(empty($this->beginDate) && empty($this->endDate)) {
            die('cannt find begin and end date');
        }

        switch($this->option) {
            case 'summary':
                $query = $this->sum_summary_query($this->beginDate, $this->endDate, $projectId);
                break;
            case 'remain':
                $query = $this->sum_still_remain_query($this->beginDate, $this->endDate);
                break;
            case 'a-1':
                $query = $this->sum_a_not_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'a-2':
                $query = $this->sum_a_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'a-3':
                $query = $this->sum_a_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            case 'a-4':
                $query = $this->sum_a_no_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            case 'b-1':
                $query = $this->sum_b_not_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'b-2':
                $query = $this->sum_b_contact_sql($this->beginDate, $this->endDate);
                break;
            case 'b-3':
                $query = $this->sum_b_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            case 'b-4':
                $query = $this->sum_b_no_remain_finish_sql($this->beginDate, $this->endDate);
                break;
            default:
                $query = $this->sum_summary_query($this->beginDate, $this->endDate);
                break;
        }
        return $query;
    }

    function queryFetchData($page, $limit) {
        if(empty($this->beginDate) && empty($this->endDate)) {
            die('cannt find begin and end date');
        }
        $from = ($page-1) * $limit;
        switch($this->option) {
            case 'summary':
                $query = $this->summary_query($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'remain':
                $query = $this->still_remain_query($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'a-1':
                $query = $this->a_not_contact_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'a-2':
                $query = $this->a_contact_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'a-3':
                $query = $this->a_remain_finish_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'a-4':
                $query = $this->a_no_remain_finish_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'b-1':
                $query = $this->b_not_contact_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'b-2':
                $query = $this->b_contact_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'b-3':
                $query = $this->b_remain_finish_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            case 'b-4':
                $query = $this->b_no_remain_finish_sql($this->beginDate, $this->endDate, $from, $limit);
                break;
            default:
                $query = $this->summary_query($this->beginDate, $this->endDate, $from, $limit);
                break;
        }
        return $query;
    }

    public function displayHTML($page, $limit) {
        $items = $this->fetchData($page, $limit);
        if($this->option != 'summary') {
            $this->displaySumHTML($this->option);
        }
        foreach ($items as $item) {
            echo "<tr>".PHP_EOL;
            foreach ($item as $key => $value) {
                echo "<td>$value</td>";
            }
            echo "</tr>".PHP_EOL;
        }
    }

    private function displaySumHTML($option) {
        $sums = $this->fetchSum($this->option);
        if(isset($sums)) {
            echo "<tr>".PHP_EOL;
            echo "<td>รวม</td>";
            echo "<td></td>";
            echo "<td>".convertNumber($sums['sum_budget'])."</td>";
            echo "<td>".convertNumber($sums['sum_po'])."</td>";
            echo "<td>".convertNumber($sums['sum_disburse'])."</td>";
            echo "<td>".convertNumber($sums['sum_remain'])."</td>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td></td>";
            echo "<td></td>";
            echo "</tr>".PHP_EOL;
        }
    }

    public function fetchSum($option = '', $projectId = 'all') {
        $conn = Database::connect();
        $query = $this->querySubtotal();
        $result = $conn->query($query);
        if(!$result) {
            echo $query;
            die('cant get sum '.$conn->error);
        }

        $data = mysqli_fetch_assoc($result);
        return $data;
    }

    public function fetchSummaryProjects() {
        $conn = Database::connect();
        $query = "SELECT id, name FROM list_project";
        $result = $conn->query($query);
        if($result) {
            $projects = array();
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($projects, $row);
            }
            mysqli_free_result($result);
            $i = 0;
            $items = array();
            foreach ($projects as $project) {
                $query = $this->sum_summary_query($this->beginDate, $this->endDate, $project['id']);
                $result = $conn->query($query);
                if($result) {
                    $sum = mysqli_fetch_assoc($result);
                    $data = array(
                        'id'=> $project['id'],
                        'name'=> $project['name'],
                        'sum'=> $sum
                    );
                    $items[$project['id']] = $data;
                }
                $i++;
            }
            return $items;
        } else {
            return null;
        }
    }

    ///////////////////////  FETCH //////////////////////////////

    private function summary_query($beginDate, $endDate, $from, $limit) {
        $query = "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain , a.agency, m.province, g.financial_amount mbudget, m.request_id, m.project_id
                    FROM list_main_data m
                    LEFT JOIN gf_list_data g
                        ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data as a
                        ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    GROUP BY m.id
                    ORDER BY m.project_id, m.request_id
                    LIMIT $from, $limit";
        return $query;
    }

    private function still_remain_query($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain , a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND (g.po>0 OR g.disburse>0) AND (g.budget - g.po - g.disburse) > 0
                GROUP BY m.id
                ORDER BY remain  DESC
                LIMIT $from, $limit";
    }

    //////  A

    private function a_not_contact_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain, a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse=0
                GROUP BY m.id
                ORDER BY m.budget  DESC
                LIMIT $from, $limit";
    }

    private function a_contact_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain, a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po>0
                GROUP BY m.id
                ORDER BY remain  DESC
                LIMIT $from, $limit";
    }

    private function a_remain_finish_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain, a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) > 0
                GROUP BY m.id
                ORDER BY remain  DESC
                LIMIT $from, $limit";
    }

    private function a_no_remain_finish_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain, a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) = 0
                GROUP BY m.id
                ORDER BY remain  DESC
                LIMIT $from, $limit";
    }

    ///// B

    private function b_not_contact_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain , a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse=0
                GROUP BY m.id
                ORDER BY m.budget  DESC
                LIMIT $from, $limit";
    }

    private function b_contact_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain , a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po>0
                GROUP BY m.id
                ORDER BY remain  DESC
                LIMIT $from, $limit";
    }

    private function b_remain_finish_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain, a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) > 0
                GROUP BY m.id
                ORDER BY remain  DESC
                LIMIT $from, $limit";
    }

    private function b_no_remain_finish_sql($beginDate, $endDate, $from, $limit) {
        return "SELECT m.id, m.name, g.budget, g.po, g.disburse, (g.budget - g.po - g.disburse) remain, a.agency, m.province, g.financial_amount mbudget, m.request_id
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) = 0
                GROUP BY m.id
                ORDER BY remain  DESC
                LIMIT $from, $limit";
    }

    ////////////////////  COUNT ///////////////////////////

    private function count_summary_query($beginDate, $endDate) {
        $query = "SELECT COUNT(m.id) AS total
                    FROM (
                        SELECT m.id FROM list_main_data m
                        LEFT JOIN gf_list_data g ON m.id = g.list_id
                        LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                        WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                        GROUP BY m.id
                    ) AS m";
        return $query;
    }

    private function count_summary_project_query($beginDate, $endDate, $projectId) {
        return $query = "SELECT count(m.id) as total
                    FROM (
                        SELECT m.id FROM list_main_data m
                        LEFT JOIN gf_list_data g ON m.id = g.list_id
                        LEFT JOIN list_agency_main_data as a ON m.id = a.list_id
                        WHERE m.project_id = $projectId AND g.submited_date BETWEEN '$beginDate' AND '$endDate'
                        GROUP BY m.id
                    ) as m";
    }

    private function count_still_remain_query($beginDate, $endDate) {
        return "SELECT count(m.id) as total
                    FROM (
                        SELECT m.id FROM list_main_data m
                        LEFT JOIN gf_list_data g ON m.id = g.list_id
                        LEFT JOIN list_agency_main_data as a ON m.id = a.list_id
                        WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND (g.po>0 OR g.disburse>0) AND (g.budget - g.po - g.disburse) > 0
                        GROUP BY m.id
                    ) as m";
    }

    ///// A

    private function count_a_not_contact_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                    FROM (
                        SELECT m.id FROM list_main_data m
                        LEFT JOIN gf_list_data g ON m.id = g.list_id
                        LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                        WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                        AND g.budget < 2000000 AND g.po=0 AND g.disburse=0
                        GROUP BY m.id
                    ) as m";
    }

    private function count_a_contact_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                    FROM (
                        SELECT m.id FROM list_main_data m
                        LEFT JOIN gf_list_data g ON m.id = g.list_id
                        LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                        WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                        AND g.budget < 2000000 AND g.po>0
                        GROUP BY m.id
                    ) as m";
    }

    private function count_a_remain_finish_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                FROM (
                    SELECT m.id FROM list_main_data m
                    LEFT JOIN gf_list_data g ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) > 0
                    GROUP BY m.id
                ) as m";
    }

    private function count_a_no_remain_finish_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                FROM (
                    SELECT m.id FROM list_main_data m
                    LEFT JOIN gf_list_data g ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) = 0
                    GROUP BY m.id
                ) as m";
    }

    ///// B
    private function count_b_not_contact_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                FROM (
                    SELECT m.id FROM list_main_data m
                    LEFT JOIN gf_list_data g ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse=0
                    GROUP BY m.id
                ) as m";
    }

    private function count_b_contact_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                FROM (
                    SELECT m.id FROM list_main_data m
                    LEFT JOIN gf_list_data g ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po>0
                    GROUP BY m.id
                ) as m";
    }

    private function count_b_remain_finish_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                FROM (
                    SELECT m.id FROM list_main_data m
                    LEFT JOIN gf_list_data g ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) > 0
                    GROUP BY m.id
                ) as m";
    }

    private function count_b_no_remain_finish_sql($beginDate, $endDate) {
        return "SELECT COUNT(m.id) AS total
                FROM (
                    SELECT m.id FROM list_main_data m
                    LEFT JOIN gf_list_data g ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data AS a ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) = 0
                    GROUP BY m.id
                ) as m";
    }

    ////////////////////////  TOTAL ///////////////////////

    private function sum_summary_query($beginDate, $endDate, $projectId = 'all') {
        $whereQuery = (empty($projectId) || $projectId == 'all') ? '' : "AND m.project_id=$projectId";
        $query = "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain 
                FROM (
                    SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                    FROM list_main_data m
                    LEFT JOIN gf_list_data g
                        ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data as a
                        ON m.id = a.list_id
                    WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate' $whereQuery
                    GROUP BY m.id
                ) as s";
        return $query;
    }

    private function sum_summary_project_query($beginDate, $endDate, $projectId) {
        return $query = "SELECT sum(g.budget) sum_budget, sum(g.po) sum_po, sum(g.disburse) sum_disburse, sum(g.budget - g.po - g.disburse)  sum_remain
                    FROM list_main_data m
                    LEFT JOIN gf_list_data g
                        ON m.id = g.list_id
                    LEFT JOIN list_agency_main_data as a
                        ON m.id = a.list_id
                    WHERE m.project_id = $projectId AND g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    GROUP BY m.id";
    }

    private function sum_still_remain_query($beginDate, $endDate) {
        return "SELECT sum(g.budget) sum_budget, sum(g.po) sum_po, sum(g.disburse) sum_disburse, sum(g.budget - g.po - g.disburse)  sum_remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND (g.po>0 OR g.disburse>0) AND (g.budget - g.po - g.disburse) > 0
                GROUP BY m.id";
    }

    ///// A

    private function sum_a_not_contact_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse=0
                GROUP BY m.id
            ) as s";
    }

    private function sum_a_contact_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po>0
                GROUP BY m.id
            ) as s";
    }

    private function sum_a_remain_finish_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) > 0
                GROUP BY m.id
            ) as s";
    }

    private function sum_a_no_remain_finish_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget < 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) = 0
                GROUP BY m.id
            ) as s";
    }

    ///// B
    private function sum_b_not_contact_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse=0
                GROUP BY m.id
            ) as s";
    }

    private function sum_b_contact_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po>0
                    GROUP BY m.id
            ) as s";
    }

    private function sum_b_remain_finish_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) > 0
                GROUP BY m.id
            ) as s";
    }

    private function sum_b_no_remain_finish_sql($beginDate, $endDate) {
        return "SELECT sum(s.budget) sum_budget, sum(s.po) sum_po, sum(s.disburse) sum_disburse, sum(s.remain)  sum_remain
            FROM (
                SELECT g.budget, g.po, g.disburse, g.budget - g.po - g.disburse  remain
                FROM list_main_data m
                LEFT JOIN gf_list_data g
                    ON m.id = g.list_id
                LEFT JOIN list_agency_main_data as a
                    ON m.id = a.list_id
                WHERE g.submited_date BETWEEN '$beginDate' AND '$endDate'
                    AND g.budget >= 2000000 AND g.po=0 AND g.disburse > 0 AND (g.budget - g.po - g.disburse) = 0
                GROUP BY m.id
            ) as s";
    }
}
