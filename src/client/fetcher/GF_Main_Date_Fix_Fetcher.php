<?php
include_once('Fetcher.php');
/**
 *
 */
class GF_Main_Date_Fix_Fetcher extends Fetcher {
    public function queryNumberOfItems() {
        return 'SELECT COUNT(DISTINCT g.id) as total
                FROM gf_list_data g
                WHERE g.validated = false';
    }

    function queryFetchData($page, $limit) {
        $from = ($page-1) * $limit;
        // $query = 'SELECT *
        //             FROM gf_list_data
        //             WHERE list_id IN
        //                 (SELECT DISTINCT list_id
        //                     FROM gf_list_data
        //                     GROUP BY list_id
        //                     HAVING COUNT(*) > 1
        //                 )
        //             ORDER BY id
        //             LIMIT '.$from.', '.$limit;
        $query = "SELECT DISTINCT g.*
              FROM gf_list_data g
              WHERE g.validated = false
              ORDER BY g.id
              LIMIT $from, $limit";
        return $query;
    }

    public function displayHTML($page, $limit) {
        $items = $this->fetchData($page, $limit);
        foreach ($items as $item) {
            echo "<tr class='row-data' data-edited='0' id='row-".$item['id']."'>".PHP_EOL;
            echo "<td>
                        <input type='text' data-id='".$item['id']."' class='form-control' value='".$item['list_id']."' name='lid-".$item['id']."'>
                    </td>".PHP_EOL;
            echo "<td>
                    <label>".$item['financial_amount']."</label>
                    </td>".PHP_EOL;
            echo "<td>
                    <label>".$item['budget']."</label>
                    </td>".PHP_EOL;
            echo "<td>
                    <label>".$item['po']."</label>
                    </td>".PHP_EOL;
            echo "<td>
                    <label>".$item['disburse']."</label>
                    </td>".PHP_EOL;
            echo "<td>
                    <input type='checkbox' id='validated-".$item['id']."' name='validated-".$item['id']."'$check tabindex='-1'>
                 </td>";
            echo "</tr>".PHP_EOL;
        }
    }
}
