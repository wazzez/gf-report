<?php
require_once('src/client/fetcher/Summary_List_Report_Fetcher.php');
$option = $_GET['option'];
$date = urldecode('2016-11-20 - 2016-11-26');
$weekly_query = "";
if(isset($_GET['weekly-date-picker']) && !empty($_GET['weekly-date-picker'])) {
    $date = urldecode($_GET['weekly-date-picker']);
    $weekly_query = $date;
}
if(isset($option)) {
    $fetcher = new Summary_List_Report_Fetcher($date, $option);
    switch($option) {
        case 'remain': $title = 'เงินเหลือจ่าย - รายการ'; break;
        case 'a-1': $title = 'รายการที่ยังไม่ทำสัญญา น้อยกว่าสองล้านบาท - รายการ'; break;
        case 'a-2': $title = 'ทำสัญญาแล้วแต่เบิกจ่ายไม่เสร็จ น้อยกว่าสองล้านบาท - รายการ'; break;
        case 'a-3': $title = 'ดำเนินการเสร็จแล้วมีเงินเหลือ น้อยกว่าสองล้านบาท - รายการ'; break;
        case 'a-4': $title = 'ดำเนินการเสร็จไม่มีเงินเหลือ น้อยกว่าสองล้านบาท - รายการ'; break;
        case 'b-1': $title = 'รายการที่ยังไม่ทำสัญญา มากกว่าสองล้านบาท - รายการ'; break;
        case 'b-2': $title = 'ทำสัญญาแล้วแต่เบิกจ่ายไม่เสร็จ มากกว่าสองล้านบาท - รายการ'; break;
        case 'b-3': $title = 'ดำเนินการเสร็จแล้วมีเงินเหลือ มากกว่าสองล้านบาท - รายการ'; break;
        case 'b-4': $title = 'ดำเนินการเสร็จไม่มีเงินเหลือ มากกว่าสองล้านบาท - รายการ'; break;
        default: $title='รายงานภาพรวม - รายการ';
    }
} else {
    $fetcher = new Summary_List_Report_Fetcher($date);
    $title='รายงานภาพรวม - รายการ';
}

$limit = 20;
include('header.php');
?>
        <div class="jumbotron">
          <div class="container">
            <h2><?php echo $title;?></h2>
            <h3>วันที่ <?php echo $date ?></h3>
          </div>
        </div>
        <div class='container'>
             <div class="row">
                <div class="col-md-6">
                    <form id='weekly-picker-form' action='' method="get" accept-charset="utf-8">
                        <div class="form-group">
                            <label for="weekly-date-picker">วันที่</label>
                            <input type="text" class="form-control" id="weekly-date-picker" name="weekly-date-picker" placeholder="" required>
                        </div>
                        <input type="text" name="option" value="<? echo $option; ?>" hidden='true'>
                        <button type="submit" class="btn btn-default" style="margin-top:20px;">Submit</button>
                    </form>
                </div>
            </div>
            <div class='row' style="margin-top: 30px">
                <div class="col-md-12">
                    <a class="btn btn-default" href="download/report.php?option=<? echo $option ?>&date=<? echo $date ?>" target="_blank">Export Excel</a>
                </div>
            </div>
            <div class='row'>
                <div class="col-md-12">
                    <h3>จำนวน <span id="number-of-items">loading...</span></h3>
                </div>
            </div>
            <div>
        		<table class="table table-condensed table-bordered">
        			<thead>
        				<tr>
        					<th>รหัสงบประมาณ</th>
        					<th>ชื่อรายการ</th>
        					<th>งบที่ได้รับจัดสรร</th>
                            <th>ใบสั่งซื้อ (PO)</th>
        					<th>เบิกจ่ายทั้งสิ้น</th>
        					<th>คงเหลือ</th>
                            <th>หน่วยเบิก</th>
                            <th>จังหวัด</th>
                            <th>วงเงินตาม พ.ร.บ.</th>
                            <th>รหัสคำขอ</th>
        				</tr>
        			</thead>
        			<tbody id="content">

        			</tbody>
        		</table>
                <div class="loader"></div>
        	</div>

        	<div id="page-selection"></div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="../../js/vendor/moment.js"></script>
        <script src="../../js/vendor/bootstrap.min.js"></script>
        <script src="../../js/vendor/bootstrap-datetimepicker.js"></script>
        <script src="../../js/weekly-date-picker.js"></script>
        <script src="js/vendor/jquery.bootpag.min.js"></script>
        <style>
            .bootstrap-datetimepicker-widget tr:hover {
                background-color: #808080;
            }
        </style>
        <script>
            // init bootpag
            var encodeDate = "<? echo urlencode($date); ?>";
            var option = "<? echo $option; ?>";

            $(document).ready(function () {
                getReport().then(getNumberOfItems).then(getNumberOfPages);
            });

            function getReport() {
                var url = 'report-list-summary-ajax.php?action=page&page=1&date='+encodeDate+'&option='+option

                return $.ajax({
                    async: true,
                    cache: false,
                    type: "GET",
                    url: url
                }).done(function (data) {
                    if (data != null) {
                        $('#content').html(data);
                    }
                });
            }

            function getNumberOfItems() {
                var url =  'report-list-summary-ajax.php?action=number-of-items&date='+encodeDate+'&option='+option;
                return $.ajax({
                    async: true,
                    cache: false,
                    type: "GET",
                    url: url
                }).done(function (data) {
                    if (data != null) {
                        var numOfItem = data['number-of-items'];
                        $('#number-of-items').html(numOfItem);
                    }
                });
            }

            function getNumberOfPages() {
                var url = 'report-list-summary-ajax.php?action=number-of-pages&date='+encodeDate+'&option='+option;
                return $.ajax({
                    async: true,
                    cache: false,
                    type: "GET",
                    url: url
                }).done(function (data) {
                    if (data != null) {
                        $('.loader').hide();
                        var numPages = data['number-of-pages'];
                        $('#page-selection').bootpag({
                            total: numPages,
                            maxVisible: 10
                        }).on("page", function(event, /* page number here */ num){
                             $("#content").html('');
                             $('.loader').show();
                             $("#content").load('report-list-summary-ajax.php?action=page&page='+num+'&limit=<? echo $limit; ?>&date='+encodeDate+'&option='+option, function() {
                                  $('.loader').hide();
                             }); // some ajax content loading...
                        });
                    }
                });
            }
        // submit date
        // $('#weekly-picker-form').submit(function(e) {
        //     var date = encodeURIComponent($('#weekly-date-picker').val());
        //     var url = 'report-list-summary-ajax.php?date='+date+'&option=<? echo $option; ?>';
        //     $("#content").load(url); // some ajax content loading...

        //     e.preventDefault(); // avoid to execute the actual submit of the form.
        // });
    </script>
    </body>
</html>
