<?php
include_once('src/server/database/database.php');
require_once('./src/client/fetcher/Main_Data_Fix_Fetcher.php');
$fetcher = new Main_Data_Fix_Fetcher;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 12000);
ini_set('max_input_time', '-1');
ini_set('max_input_vars', 10000);

if(!isset($_POST) || empty($_POST)) {
    header("Location: main-data-fix.php");
    die();
}

$conn = Database::connect();

foreach($_POST as $key=>$value) {
    if (strpos($key, 'budget-') !== FALSE) {
        $id_componet = explode('-',$key);
        if(count($id_componet) != 2) { continue; }
        $id = $id_componet[1];

        if(!isset($_POST["validated-$id"])) { continue; }

        $budget     = str_replace(',', '', trim($value));
        $budget     = floatval($budget);

        $name       = $_POST['name-'.$id];
        $project_id = $_POST['project-'.$id];
        $year       = intval($_POST['year-'.$id]);
        $request_id = intval($_POST['request_id-'.$id]);

        $agency_id  = $_POST['agency_id-'.$id];
        $agency     = $_POST['agency-'.$id];

        $query = "REPLACE INTO list_main_data
                (id, name, budget, request_id, year, project_id)
                VALUES ('$id', '$name', $budget, $request_id, $year,  '$project_id')";
        if(!$conn->query($query)) {
            echo $query.PHP_EOL;
            die('insert main data error: '.$conn->error);
        }

        $query = "REPLACE INTO list_agency_main_data
                (list_id, list_name, agency, agency_id, validated)
                VALUES ('$id', '$name', '$agency', '$agency_id', 1)";

        if(!$conn->query($query)) {
            echo $query.PHP_EOL;
            die('insert additional main data error: '.$conn->error);
        }
    }
}

if($fetcher->numberOfItems() > 0) {
    header("Location: main-data-fix.php");
} else {
    header("Location: list-main-data.php");
}
die();
