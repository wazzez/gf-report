<?php

include_once('src/server/database/database.php');
include_once('src/client/fetcher/List_Agency_Fix_Fetcher.php');
include_once('./src/client/fetcher/Main_Data_Agency_Fix_Fetcher.php');

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 12000);

if(isset($_POST) && count($_POST) > 0) {
    $a = [];
    foreach($_POST as $key=>$value) {
        if(strpos($key, 'lid-') === FALSE) { continue; }
        $id_componet = explode('-',$key);
        if(count($id_componet) != 2) { continue; }
        $id = $id_componet[1];

        if(isset($_POST["validated-$id"])) {
            $conn = Database::connect();
            $query = "UPDATE list_agency_main_data
                        SET list_id = $value, validated = true
                        WHERE  id = $id";
            if(!$conn->query($query)) {
                die('update agency error: '.$conn->error);
            }
        }
    }

    $fetcher = new List_Agency_Fix_Fetcher;
    $f       = new Main_Data_Agency_Fix_Fetcher;
    if($fetcher->numberOfItems() > 0) {
        header("Location: list-additional-main-data-fix.php");
    } elseif ($f->numberOfItems() > 0) {
        header("Location: main-data-additional-fix.php");
    } else {
        header("Location: list-main-data.php");
    }
	die();
}
