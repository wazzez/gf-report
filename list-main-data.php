<?php
require_once('./src/client/fetcher/Main_Data_Fetcher.php');
$fetcher = new Main_Data_Fetcher;
if (!empty($_GET['limit'])) {
    $limit = $_GET['limit'];
} else {
    $limit = 20;
}

$title = "รายการข้อมูลหลัก";
include('./header.php');
?>
        <div class="jumbotron">
          <div class="container">
            <h2>รายการข้อมูลหลัก</h2>
          </div>
        </div>
        <div class="container progress-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        0%
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <form id="additional-data-form" action="list_additional_main_data_submit.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="year">ข้อมูลหลัก(เพิ่ม)</label>
                            <input type="file" name="excelFile" value="" placeholder="" required>
                      </div>
                      <input type="submit" name="submit" value="Submit" style="margin-bottom:20px;"/>
                    </form>
                </div>
                <div class="col-md-6">
                    <form id="area-data-form" action="list_additional_area_main_data_submit.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="year">ข้อมูลหลักเขต</label>
                            <input type="file" name="excelFile" value="" placeholder="" required>
                      </div>
                      <input type="submit" name="submit" value="Submit" style="margin-bottom:20px;"/>
                    </form>
                </div>
            </div>
          <div class="row" style="margin-bottom:5px;">
              <div class="col-md-6" >
                <a href="list-main-data-add.php">เพิ่มรายการหลัก</a>
              </div>
              <div class="col-md-6">
                <a href="list-additional-main-data-fix.php">แก้ไขข้อมูลหลัก(เพิ่ม)</a>
              </div>
          </div>
          <hr>
          <!-- <div class="row" style="margin-bottom:10px;margin-top:10px;">
              <div class="col-md-6">
                <a href="main-data-fix.php">เพิ่มรายการหลักที่มีอยู่ใน GF</a>
              </div>
          </div> -->
          <div class="row" style="margin-bottom:10px;margin-top:10px;">
              <div class="col-md-6">
                <a href="main-data-additional-fix.php">เพิ่มรายการหลักที่มีอยู่ในข้อมูลหลัก(เพิ่ม)</a>
              </div>
          </div>
          <hr>
        	<div>
        		<table class="table table-condensed table-bordered">
        			<thead>
        				<tr>
        					<th>รหัสงบประมาณ</th>
        					<th>ชื่อรายการ</th>
        					<th>จำนวนเงิน</th>
                  <th>หน่วยเบิก</th>
        					<th>จังหวัด</th>
        					<th>รหัสคำขอ</th>
        					<th>รหัสผลผลิตโครงการ</th>
        				</tr>
        			</thead>
        			<tbody id="content">
        				<? $fetcher->displayHTML(1, $limit)?>
        			</tbody>
        		</table>
        	</div>
            <div class="row">
                <div class="col-md-offset-5 col-md-1">
                    <select id="page-control" class="form-control">
                      <option value='20' <?php echo $limit==20 ? "selected" : "" ?>>20</option>
                      <option value='50' <?php echo $limit==50 ? "selected" : "" ?>>50</option>
                      <option value='100' <?php echo $limit==100 ? "selected" : "" ?>>100</option>
                      <option value='all' <?php echo $limit=='all' ? "selected" : "" ?>>ทั้งหมด</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div id="page-selection"></div>
            </div>
        </div>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/jquery.bootpag.min.js"></script>
        <script src="js/main.js"></script>
        <script>
        // init bootpag
        $('#page-selection').bootpag({
            total: <? echo $fetcher->numberOfPages($limit); ?>,
            maxVisible: 20,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
        }).on("page", function(event, /* page number here */ num){
            loadpage(num, <? echo $limit; ?>);
        });

        $('#page-control').change(function() {
            var limit = limit = $(this).val();
            window.location.href = "/list-main-data.php?limit="+limit;
        });

        function loadpage(page, limit) {
            $("#content").load('list-main-data-ajax.php?page='+page+'&limit='+limit , function() {
                $('#page-selection').bootpag({total:  <? echo $fetcher->numberOfPages($limit); ?>});
            });
        }
    </script>

    <script type="text/javascript">
        $("#additional-data-form").submit(function() {
            handleProgress();
        });

        $("#area-data-form").submit(function() {
            handleProgress();
        });
    </script>
    </body>
</html>
