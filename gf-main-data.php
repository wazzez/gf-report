<?php
require_once('./src/client/fetcher/GF_Main_Data_Fetcher.php');
if (isset($_GET['begin-date']) && isset($_GET['end_date']) ) {
    $beginDate = $_GET['begin-date'];
    $endDate = $_GET['end_date'];
} else {
    $day = date('w');
    $beginDate = date('Y-m-d', strtotime('-'.$day.' days'));
    $endDate = date('Y-m-d', strtotime('+'.(6-$day).' days'));
}

$fetcher = new GF_Main_Data_Fetcher($beginDate, $endDate);
if (!empty($_GET['limit'])) {
    $limit = $_GET['limit'];
} else {
    $limit = 20;
}
$title = "input รายการ";
include('header.php');
?>

<div class="jumbotron">
          <div class="container">
            <h2>input รายการ</h2>
          </div>
        </div>
        <div class="container progress-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        0%
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
          <div class="col-md-12">
              <div class="row">
                  <div class="col-md-6">
                      <form id="gf-main-data-form" action="gf-main-data-submit.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                          <div class="form-group">
                              <label for="year">input รายการ</label>
                              <input type="file" name="excelFile" value="" placeholder="" required>
                        </div>
                        <input type="submit" name="submit" value="Submit" style="margin-bottom:20px;"/>
                      </form>
                  </div>
                  <div class="col-md-6">
                      <p>ลบข้อมูล GF สัปดาห์นี้</p>
                      <button id='truncate-button' type="button" name="button">ลบ</button>
                  </div>
                  <div class="col-md-6" style="margin-top: 30px">
                      <a href="main-data-fix.php">เพิ่มรายการหลักที่อยู่ใน GF</a>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                      <a href="gf-main-data-fix.php">แก้ไขข้อมูล GF</a>
                  </div>
              </div>
              <hr/>
          </div>
          <div class="col-md-6">
              <h2>ข้อมูล GF</h2>
              <form id='weekly-picker-form' action='' method="get" accept-charset="utf-8">
                  <div class="form-group">
                      <label for="weekly-date-picker">วันที่</label>
                      <input type="text" class="form-control" id="weekly-date-picker" name="weekly-date-picker" placeholder="" required>
                  </div>
                  <button type="submit" class="btn btn-default" style="margin-top:20px;">Submit</button>
              </form>
              <br/>
          </div>
          <div>
          	<table class="table table-condensed table-bordered">
          		<thead>
          			<tr>
          				<th>รหัสงบประมาณ</th>
          				<th>พรบ.</th>
          				<th>จัดสรรถือจ่าย</th>
                          <th>ใบสั่งซื้อ</th>
          				<th>เบิกเอง</th>
                          <th>วันที่เก็บข้อมูล</th>
          			</tr>
          		</thead>
          		<tbody id="content">
          			<? $fetcher->displayHTML(1, $limit)?>
          		</tbody>
          	</table>
          </div>
          <div class="row">
              <div class="col-md-offset-5 col-md-1">
                  <select id="page-control" class="form-control">
                    <option value='20' <?php echo $limit==20 ? "selected" : "" ?>>20</option>
                    <option value='50' <?php echo $limit==50 ? "selected" : "" ?>>50</option>
                    <option value='100' <?php echo $limit==100 ? "selected" : "" ?>>100</option>
                    <option value='all' <?php echo $limit=='all' ? "selected" : "" ?>>ทั้งหมด</option>
                  </select>
              </div>
          </div>
          <div id="page-selection"></div>
        </div>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="../../js/vendor/moment.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/jquery.bootpag.min.js"></script>
        <script src="js/main.js"></script>
        <script src="../../js/vendor/bootstrap-datetimepicker.js"></script>
        <script src="../../js/weekly-date-picker.js"></script>
        <style>
            .bootstrap-datetimepicker-widget tr:hover {
                background-color: #808080;
            }
        </style>
        <script>
            // init bootpag
            $('#page-selection').bootpag({
                total: <? echo $fetcher->numberOfPages($limit); ?>,
                maxVisible: 20,
                leaps: true,
                firstLastUse: true,
                first: '←',
                last: '→',
            }).on("page", function(event, /* page number here */ num){
                 loadpage(num, <? echo $limit; ?>);
            });

            $('#page-control').change(function() {
                var limit = limit = $(this).val();
                window.location.href = "/gf-main-data.php?action=fetch&begin-date="+firstDate+"&end_date="+lastDate+"&limit="+limit;
            });

            function loadpage(page, limit) {
                $("#content").load("/gf-main-data-ajax.php?action=fetch&begin-date="+firstDate+"&end_date="+lastDate+"&page="+page+"&limit="+limit , function() {
                    $('#page-selection').bootpag({total:  <? echo $fetcher->numberOfPages($limit); ?>});
                });
            }

            $('#weekly-picker-form').submit(function() {
                window.location='/gf-main-data.php?begin-date='+firstDate+'&end_date='+lastDate+"&limit=<? echo $limit; ?>";
                return false;
            })

            $('#truncate-button').click(function() {
                var r = confirm("ต้องการลบข้อมูล GF สัปดาห์นี้?");
                if (r == true) {
                    $.get('gf-main-data-ajax.php?action=delete', function(data) {
                        if (data === true) {
                            alert('ลบข้อมูลเรียบร้อย');
                             $("#content").load('gf-main-data-ajax.php?action=fetch&begin-date='+firstDate+'&end_date='+lastDate+'&page=1&limit=<? echo $limit; ?>');
                        }
                    });
                } else {
                    console.log('false');
                }
            })
    </script>
    <script type="text/javascript">
        $("#gf-main-data-form").submit(function() {
            handleProgress();
        });
    </script>
    </body>
</html>
