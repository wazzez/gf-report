<?php
require_once('../../src/client/fetcher/Summary_List_Report_Fetcher.php');
$fetcher = new Summary_List_Report_Fetcher('2016-11-13 - 2016-11-19');
$limit = 20;
$title='รายงานภาพรวม - ข้อมูลหลัก';
include('../../header.php');
?>
        <div class="jumbotron">
          <div class="container">
            <h2>รายงานภาพรวม</h2>
          </div>
        </div>
        <div class='container'>
             <div class="row">
                <div class="col-md-6">
                    <form action="submit/index-submit.php" method="get" accept-charset="utf-8">
                        <div class="form-group">
                            <label for="weekly-date-picker">วันที่</label>
                            <input type="text" class="form-control" id="weekly-date-picker" name="weekly-date-picker" placeholder="" required>
                        </div>
                        <button type="submit" class="btn btn-default" style="margin-top:20px;">Submit</button>
                    </form>
                </div>
            </div>
            <div>
        		<table class="table table-condensed table-bordered">
        			<thead>
        				<tr>
        					<th>รหัสงบประมาณ</th>
        					<th>ชื่อรายการ</th>
        					<th>จำนวนเงิน</th>
                  <th>หน่วยเบิก</th>
        					<th>จังหวัด</th>
        					<th>รหัสคำขอ</th>
        					<th>รหัสผลผลิตโครงการ</th>
        				</tr>
        			</thead>
        			<tbody id="content">
        				<? $fetcher->displayHTML(1, $limit)?>
        			</tbody>
        		</table>
        	</div>

        	<div id="page-selection"></div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="../../js/vendor/moment.js"></script>
        <script src="../../js/vendor/bootstrap.min.js"></script>
        <script src="../../js/vendor/bootstrap-datetimepicker.js"></script>
        <script src="../../js/weekly-date-picker.js"></script>
        <script src="js/vendor/jquery.bootpag.min.js"></script>
        <style>
            .bootstrap-datetimepicker-widget tr:hover {
                background-color: #808080;
            }
        </style>
        <script>
        // init bootpag
        $('#page-selection').bootpag({
            total: <? echo $fetcher->numberOfPages($limit); ?>,
            maxVisible: 10
        }).on("page", function(event, /* page number here */ num){
             $("#content").load('list-main-data-ajax.php?page='+num+'&limit=<? echo $limit; ?>'); // some ajax content loading...
        });
    </script>
    </body>
</html>