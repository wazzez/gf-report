<?php
require_once('./src/client/fetcher/Main_Data_Agency_Fix_Fetcher.php');
$fetcher = new Main_Data_Agency_Fix_Fetcher;

$page = 1;
$limit = 20;

if (isset($_GET['page'])) {
	$page = $_GET['page'];
}

if (isset($_GET['limit'])) {
	$limit = $_GET['limit'];
}

$fetcher->displayHTML($page, $limit);
