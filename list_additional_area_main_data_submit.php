<?php
require_once('./src/server/reader/List_Main_Additional_Area_Reader.php');
if($_FILES['excelFile']['name']) {
    if(!$_FILES['excelFile']['error']) {
        $target_dir = "uploads/";
        $target_file = $target_dir . rawurlencode(basename($_FILES["excelFile"]["name"]));
        if (move_uploaded_file($_FILES['excelFile']['tmp_name'], $target_file)) {
          $reader = new List_Main_Additional_Area_Reader;
          $reader->importData($target_file);
          header("Location: list-main-data.php");
          die();
        } else {
          echo "Possible file upload attack!\n";
        }
    }  else {
        echo 'Ooops!  Your upload triggered the following error:  '.$_FILES['excelFile']['error'];
    }
} else {
    echo "no file upload";
}
